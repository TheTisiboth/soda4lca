package edu.kit.soda4lca.test.ui.basic;

import static com.codeborne.selenide.Selenide.open;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.codeborne.selenide.WebDriverRunner;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * First test, just look around on the user interface and try to log in with different user/password combinations
 * 
 * @author mark.szabo
 * 
 */
public class T001Absolut_BasicTest extends AbstractUITest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T001Absolut_BasicTest.class );

	/**
	 * just look around on the user interface, if everything is there
	 */
	@Test( priority = 11 )
	public static void absolut_Basic() {
		log.info( "Absolut Basic test started" );
		WebDriverRunner.setWebDriver(TestContext.getInstance().getDriver());
		String site = TestContext.PRIMARY_SITE_URL;
		Properties lang = TestContext.lang;
		// delete previous session
		WebDriverRunner.clearBrowserCache();;

		// open the main site
		open( site );

		log.debug( "looking for menu and menu items" );
		// is the menu there?
		TestFunctions.findAndWaitOnElement( By.className("ui-menu" ) );
		// is the content "Welcome" there?
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='content']" ) );

		// are all the menuoptions there?

		// Admin area SHOULDN'T be here
		if ( TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ) ) ) )
			org.testng.Assert.fail( "Admin area button is presented without logging on." );
		// Processes
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.processes" ) ) );
		// LCIA Methods
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.lciaMethods" ) ) );
		// Elementary Flows
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.elementaryFlows" ) ) );
		// Product Flows
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.productFlows" ) ) );
		// Flow Properties
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.flowProperties" ) ) );
		// Unit Groups
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.unitGroups" ) ) );
		// Sources
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.sources" ) ) );
		// Contacts
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "common.contacts" ) ) );
		// Search Processes
		TestFunctions.findAndWaitOnElement( By.linkText( lang.getProperty( "public.search" ) + " " + lang.getProperty( "common.processes" ) ) );
		log.info( "Absolut Basic test finished" );
	}

	/**
	 * try to log in with different user/password combinations
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 12, dependsOnMethods = { "absolut_Basic" } )
	public static void basic_Login() throws InterruptedException {
		// Login test
		log.info( "Login test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		log.debug( "Basic Login test started - Try to log in with different credentials" );
//		TestFunctions.login( "admin", "default", true );
//		TestFunctions.login( "admin", "admin", false );
//		TestFunctions.login( "admin", "", false );
//		TestFunctions.login( "somebody", "default", false );
//		TestFunctions.login( "", "default", false );
//		TestFunctions.login( "somebody", "password", false );
//		TestFunctions.login( "", "", false );
		log.info( "Basic Login test finished" );
	}

}
