package edu.kit.soda4lca.test.ui.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.testng.ScreenShooter;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;
import net.java.truevfs.access.TFile;

/**
 * Test import and export
 * 
 * @author mark.szabo
 * 
 */
@Listeners({ScreenShooter.class})
public class T015ImportExportTest extends AbstractUITest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T015ImportExportTest.class );

	@Override
	protected List<List<String>> getDBDataSetFileName() { return Arrays.asList(Arrays.asList("DB_pre_T015ImportExportTest.xml")); };

	/**
	 * Import data from a file (specified in the Main as datafilepath)
	 * 
	 * @throws InterruptedException
	 */
	 @Test( priority = 151) 
	public void importData() throws InterruptedException {
		log.info( "Import test started" );
		// import some data
		WebDriverRunner.clearBrowserCache();
		
		// login as admin
		TestFunctions.login("admin", "default", true, true);
		// click on Admin area
        TestFunctions.gotoAdminArea();
		// Hover over the menu
		$( By.linkText( TestContext.lang.getProperty( "admin.dataImport" ) ) ).hover();
		// Click Import
		$( By.linkText( TestContext.lang.getProperty( "admin.import" ) ) ).click();
		// choose the file (specified in the beginning of this code)
		String path = this.getClass().getResource( TestContext.datafilepath ).getPath();
		// if we are on a windows maschine, the first / before the C:/ should be removed
		if ( File.separator.equals( "\\" ) )
			path = path.substring( 1 );
		path = path.replace( "/", java.io.File.separator );
		log.info( "loading file " + path );
		// wait for the site to load
		$x( ".//*[@id='admin_footer']" ).shouldBe(visible);
		log.info( "found footer, sending keys " );
		$x( ".//*[@id='documentToUpload_input']" ).setValue( path );

		// click Upload
		// it's not a link but a button. So a little bit diMain.getInstance().getDriver()erent code
		log.info( "about to click 'upload'" );
		$x( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.fileUpload.upload" ) + "')]" ).click();
		
		$x( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.fileUpload.upload" ) + "')]" ).shouldBe(disabled);
		
		// if uploaded successfully click 'Continue to step 2 (import)'
		for (int i = 0; i < TestContext.timeout && ! $x(".//*[@id='admin_content']/h1").text().equals((TestContext.lang.getProperty("admin.importUpload.step2"))); i++)
		    $x( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.importUpload.goToStep2" ) + "')]" ).click();
		// wait for the site to load
	    $x(".//*[@id='admin_content']/h1").shouldHave(exactText(TestContext.lang.getProperty("admin.importUpload.step2")));
		
	    
	    // choose root data stock
//	    for (int i = 0; i < TestContext.timeout && !$x(".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[1]").isDisplayed(); i++)
//	        $x( ".//*[@id='tableForm:importRootDataStock_label']" ).click();
//		int i = 1;
//		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[" + i + "]" ) ).getText().equals(
//				"RootStock1" ) )
//			i++;
//		$x( ".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[" + i + "]" ).click();
	    
	    // choose root data stock
	    
	    $x(".//*[@id='tableForm:importRootDataStock_label']").click(); // open stock selection menu
//	    TestFunctions.waitUntilSiteLoaded();
	    Thread.sleep(TestContext.wait * 5);
	    $x(".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[contains(.,'RootStock1')]").click(); // click on RootStock1
		
//		TestFunctions.waitUntilSiteLoaded();
//	    Thread.sleep(TestContext.wait * 5);
	    
		WebDriverWait wait = new WebDriverWait(WebDriverRunner.getWebDriver(), 5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//button[contains(.,'" + TestContext.lang.getProperty("admin.importFiles") + "')]")));

		// next site, click on Import files
		$x( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.importFiles" ) + "')]" ).click();

		log.debug( "Import started, it could take a while" );
		// then it starts importing. Status log is on an iframe, so change the driver
		switchTo().frame( "console" );
		
		TestFunctions.findAndWaitOnConsoleElement( By.xpath( "html/body[contains(.,'------ import of files finished ------')]" ) );
		
		log.debug( "Import finished" );
		
		if ( !WebDriverRunner.getWebDriver().getPageSource().contains( "------ import of files finished ------" ) )
			org.testng.Assert.fail( "Importing failed. For more deatils, see the log: " + WebDriverRunner.getWebDriver().getPageSource() );
		switchTo().defaultContent();
		log.info( "Import test finished" );
	}

	/**
	 * 
	 * Export entire dataset, then save automatically (with firefox profile, see Main). Then with TFile compare some
	 * xmls
	 * inside the zip with stored ones
	 * 
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	// @Test( priority = 152, dependsOnMethods = { "importData" } )
	public void exportData() throws MalformedURLException, URISyntaxException, IOException, InterruptedException {
		// export Database
		log.info( "Export test started" );

		// first delete the donwloaded file, in case of previous test failed and the file is still there
		if ( this.getClass().getResourceAsStream( "/tmp/_Node.zip" ) != null ) {
			String downld = this.getClass().getResource( "/tmp/_Node.zip" ).getPath();
			if ( File.separator.equals( "\\" ) )
				downld = downld.substring( 1 );
			downld = downld.replace( "/", java.io.File.separator );

			File zip = new File( downld );
			zip.delete();
			if ( this.getClass().getResourceAsStream( "/tmp/_Node.zip" ) != null )
				org.testng.Assert.fail( "Exported file is still there from previous export and can not be deleted. Path: "
						+ this.getClass().getResource( "/tmp/_Node.zip" ).getPath() );
		}

		TestContext.getInstance().getDriver().manage().deleteAllCookies();
		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.gotoAdminArea();
		
		// wait for the site to load
		TestFunctions.waitUntilSiteLoaded();
		// Hover over the menu
		$( By.linkText( TestContext.lang.getProperty( "admin.dataImport" ) ) ).hover();
		// Click Export
		$( By.linkText( TestContext.lang.getProperty( "admin.export" ) ) ).click();
		// Click on 'Export entire database' button
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.export.db" ) + "')]" ) ).click();
		// the download will begin and the file is going to get saved in project folder /tmp
		log.debug( "Download started" );

		// wait for the download to finish
		int i = 0;
		boolean found = false;
		// TODO
		// problem here, when I check the file, it can interrupt the download. So I wait 10 sec between checks
		// but if the test fails here, it is still possible, that everything working fine, just this problem occurred
		while ( !found && i < 12 ) {
			Thread.sleep( 10000 );
			i++;
			found = true;
			try {
				String downld = this.getClass().getResource( "/tmp/_Node.zip" ).getPath() + "/processes/2dbe076f-4a31-4348-a36a-bedf27f56715.xml";
				// if we are on a windows machine, the first / before the C:/ should be removed
				if ( File.separator.equals( "\\" ) )
					downld = downld.substring( 1 );
				downld = downld.replace( "/", java.io.File.separator );
				TFile fileInsideArchive = new TFile( downld );
				BufferedReader f = new BufferedReader( new InputStreamReader( new FileInputStream( fileInsideArchive ) ) );
				f.close();
			}
			catch ( Exception e ) {
				found = false;
			}

		}

		if ( i == 120 )
			org.testng.Assert
					.fail( "Downloading file at export timeout. File isn't available after 120 second. Or download interrupted when check the avaibility of the file" );

		log.debug( "Download finished, check it" );

		String downloaded[] = { "/processes/2dbe076f-4a31-4348-a36a-bedf27f56715.xml", "/lciamethods/8bc7e4c1-da82-43d5-8d6c-983c8e876dc8.xml",
				"/flows/00b6b4bc-a3a5-4b12-ba67-4b4e76bd26bd.xml", "/flows/de8e110e-4873-4557-8304-b924f92686df.xml",
				"/flowproperties/cefa9508-86b9-4fad-824e-bbf9d6fdd6f9.xml", "/unitgroups/06bd04f7-338e-446e-9711-c09d67114dac.xml",
				"/sources/d744d262-ca73-4d9f-9f37-21a8e5c30434.xml", "/contacts/5bb337b0-9a1a-11da-a72b-0800200c9a70.xml" };
		String original[] = { "excavator", "ilcd2011", "diazabicyclooctane", "BOF", "arsenic", "recipe", "worldsteel", "Boustead" };
		for ( i = 0; i < 8; i++ ) {
			String downld = this.getClass().getResource( "/tmp/_Node.zip" ).getPath() + downloaded[i];
			// if we are on a windows machine, the first / before the C:/ should be removed
			if ( File.separator.equals( "\\" ) )
				downld = downld.substring( 1 );
			downld = downld.replace( "/", java.io.File.separator );
			TFile fileInsideArchive = new TFile( downld );
			BufferedReader on = new BufferedReader( new InputStreamReader( new FileInputStream( fileInsideArchive ) ) );
			// open the original xml file
			// get project path
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			// open the file from the project/src/xmls
			BufferedReader off = new BufferedReader( new InputStreamReader( loader.getResourceAsStream( ".\\reference-xmls\\" + original[i] + ".xml" ) ) );
			String line1;
			String line2;
			// compare line by line the stored and the downloaded file
			while ( (line1 = on.readLine()) != null ) {
				line2 = off.readLine();
				if ( !line1.equals( line2 ) )
					org.testng.Assert.fail( "Export error. The stored and the downloaded xml file isn't the same. Difference: '" + line1 + "' vs '" + line2
							+ "'" );
			}
			on.close();
			off.close();
		}
		// delete the downloaded file
		String downld = this.getClass().getResource( "/tmp/_Node.zip" ).getPath();
		File zip = new File( downld );
		zip.delete();
		log.info( "Export test finished" );
	}

}
