package de.iai.ilcd.db.migrations;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.googlecode.flyway.core.api.migration.spring.SpringJdbcMigration;

/**
 * Randomize initial default data stock UUID
 */
public class V1_1__DataStockUUID implements SpringJdbcMigration {

	/**
	 * Logger to use
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( V1_1__DataStockUUID.class );

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void migrate( JdbcTemplate jdbcTemplate ) throws Exception {

		V1_1__DataStockUUID.LOGGER.info( "Randomize initial default data stock UUID." );

		final String sqlSetUUID = "UPDATE `datastock` SET `UUID`=? WHERE `ID`=?";

		// set random UUID for default data stock
		BatchPreparedStatementSetter pstSet = new BatchPreparedStatementSetter() {

			@Override
			public void setValues( PreparedStatement ps, int i ) throws SQLException {
				ps.setString( 1, UUID.randomUUID().toString() );
				ps.setLong( 2, 1L );
			}

			@Override
			public int getBatchSize() {
				return 1;
			}
		};
		jdbcTemplate.batchUpdate( sqlSetUUID, pstSet );
	}

}
