package de.iai.ilcd.xml.read;

import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.contact.Contact;
import de.iai.ilcd.model.dao.AbstractDigitalFileProvider;
import de.iai.ilcd.model.dao.AbstractDigitalFileProvider.FileSystemDigitalFileProvider;
import de.iai.ilcd.model.dao.ContactDao;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.FlowPropertyDao;
import de.iai.ilcd.model.dao.LCIAMethodDao;
import de.iai.ilcd.model.dao.LifeCycleModelDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.dao.SourceDao;
import de.iai.ilcd.model.dao.UnitGroupDao;
import de.iai.ilcd.model.datastock.RootDataStock;
import de.iai.ilcd.model.flow.Flow;
import de.iai.ilcd.model.flowproperty.FlowProperty;
import de.iai.ilcd.model.lciamethod.LCIAMethod;
import de.iai.ilcd.model.lifecyclemodel.LifeCycleModel;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.model.source.Source;
import de.iai.ilcd.model.unitgroup.UnitGroup;
import de.iai.ilcd.xml.read.DataSetImporter.DaoProvider;
import de.iai.ilcd.xml.read.DataSetImporter.FlowDaoProvider;
import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;
import net.java.truevfs.access.TVFS;

/**
 * Importer for ILCD zip files
 */
public class DataSetZipImporter {

	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger( DataSetZipImporter.class );

	/**
	 * Data set importer
	 */
	private final DataSetImporter dsImporter;

	/**
	 * Create the ZIP importer for ILCD zip files
	 * 
	 * @param dsImporter
	 *            data set importer
	 */
	public DataSetZipImporter( DataSetImporter dsImporter ) {
		super();
		if ( dsImporter == null ) {
			throw new IllegalArgumentException( "Data set importer must not be null!" );
		}
		this.dsImporter = dsImporter;
	}

	/**
	 * Import a ZIP file
	 * 
	 * @param zipFileName
	 *            name of the zip file
	 * @param out
	 *            output writer
	 * @param rds
	 *            root data stock
	 */
	public void importZipFile( String zipFileName, PrintWriter out, RootDataStock rds ) {
		logger.debug( "importing ZIP archive " + zipFileName );

		TFile zipFile = new TFile( zipFileName );

		if ( !zipFile.isArchive() ) {
			out.println( "Error: file " + zipFileName + " is not a zip archive!" );
			out.println( "Please upload only ILCD conformant zip archives." );
			out.flush();
			return;
		}

		// we try to parse several different zip file formats
		// we allow data sets to be directly in subdirectories, like processes, etc. in the root directory of the zip
		// file
		// or data sets directories are under a subdirectory. Official ILCD zip files have them in "ILCD" or "ilcd"
		// subdirectories.
		// So this case is also handled
//		String subDir = this.findDataSubDirectory( zipFile, zipFileName );
		String absDir = this.findDataAbsDirectory(zipFile, 4);

		ContactReader contactReader = new ContactReader();
		DaoProvider<Contact, ContactDao> contactDaoProvider = new DaoProvider<Contact, ContactDao>( new ContactDao() );
		this.processDataSetDirectory( absDir + "/contacts", "contact", DataSetType.CONTACT, contactReader, contactDaoProvider, rds, out, null );
		
		SourceReader sourceReader = new SourceReader();
		DaoProvider<Source, SourceDao> sourceDaoProvider = new DaoProvider<Source, SourceDao>( new SourceDao() );
		// trailing slash important!
		FileSystemDigitalFileProvider digitFileProvider = new FileSystemDigitalFileProvider( absDir + "/sources/" );
		this.processDataSetDirectory(  absDir + "/sources", "source", DataSetType.SOURCE, sourceReader, sourceDaoProvider, rds, out, digitFileProvider );

		UnitGroupReader unitGroupReader = new UnitGroupReader();
		DaoProvider<UnitGroup, UnitGroupDao> unitGroupDaoProvider = new DaoProvider<UnitGroup, UnitGroupDao>( new UnitGroupDao() );
		this.processDataSetDirectory(  absDir + "/unitgroups", "unit group", DataSetType.UNITGROUP, unitGroupReader, unitGroupDaoProvider, rds, out, null );

		FlowPropertyReader flowPropertyReader = new FlowPropertyReader();
		DaoProvider<FlowProperty, FlowPropertyDao> flowpropDaoProvider = new DaoProvider<FlowProperty, FlowPropertyDao>( new FlowPropertyDao() );
		this.processDataSetDirectory(  absDir + "/flowproperties", "flow properties", DataSetType.FLOWPROPERTY, flowPropertyReader, flowpropDaoProvider, rds, out, null );

		FlowReader flowReader = new FlowReader();
		FlowDaoProvider<Flow> flowDaoProvider = new FlowDaoProvider<Flow>();
		this.processDataSetDirectory(  absDir + "/flows", "flow", DataSetType.FLOW, flowReader, flowDaoProvider, rds, out, null );

		LCIAMethodReader lciamethodReader = new LCIAMethodReader();
		DaoProvider<LCIAMethod, LCIAMethodDao> lciaMethodDaoProvider = new DaoProvider<LCIAMethod, LCIAMethodDao>( new LCIAMethodDao() );
		this.processDataSetDirectory(  absDir + "/lciamethods", "LCIA method", DataSetType.LCIAMETHOD, lciamethodReader, lciaMethodDaoProvider, rds, out, null );

		ProcessReader processReader = new ProcessReader();
		DaoProvider<Process, ProcessDao> processDaoProvider = new DaoProvider<Process, ProcessDao>( new ProcessDao() );
		this.processDataSetDirectory( absDir + "/processes", "process", DataSetType.PROCESS, processReader, processDaoProvider, rds, out, null );
		
		LifeCycleModelReader lifeCycleModelReader = new LifeCycleModelReader();
		DaoProvider<LifeCycleModel, LifeCycleModelDao> lifeCycleModelDaoProvider = new DaoProvider<LifeCycleModel, LifeCycleModelDao>( new LifeCycleModelDao() );
		this.processDataSetDirectory( absDir + "/lifecyclemodels", "lifecyclemodel", DataSetType.LIFECYCLEMODEL, lifeCycleModelReader, lifeCycleModelDaoProvider, rds, out, null );


		// flush output at end;
		if ( out != null ) {
			out.flush();
		}

		// don't forget to unmount zipFile and release resources of it
		try {
			TVFS.umount( zipFile );
		}
		catch ( Exception e ) {
			// do nothing, we just close all zip archive file resources
			logger.error( "error unmounting zipfile", e );
		}

	}

	/**
	 * @param <T>
	 * @param zipDirectory
	 * @param type
	 * @param reader
	 * @param daoProvider
	 * @param out
	 * @param rds
	 * @param digitFileProvider
	 */
	private <T extends DataSet> void importFiles( TFile zipDirectory, DataSetType type, DataSetReader<T> reader,
			DaoProvider<T, ? extends DataSetDao<T, ?, ?>> daoProvider, PrintWriter out, RootDataStock rds, AbstractDigitalFileProvider digitFileProvider ) {
		TFile[] files = null;

		int filesImported = 0;
		int filesNotImported = 0;

		try {
			files = (TFile[]) zipDirectory.listFiles();

			if ( out != null ) {
				logger.debug( "importing from " + zipDirectory.getAbsolutePath() );
				out.println( "Number of files to import: " + files.length );
				out.flush();
			}

			int i = 1;
			for ( TFile file : files ) {

				boolean persisted = this.dsImporter.importDataSet( type, file.getName(), new TFileInputStream( file ), out, rds, digitFileProvider ) != null;
				if ( out != null ) {
					if ( persisted ) {
						out.println( "Imported file (" + i + "/" + files.length + "): " + file.getName() );
						filesImported++;
					}
					else {
						out.println( "FILE WAS NOT IMPORTED: " + file.getName() );
						filesNotImported++;
					}
					out.flush();
				}
				i++;
			}
		}
		catch ( Exception e ) {
			logger.error( "Cannot import all files from directory: {}", zipDirectory.getAbsolutePath() );
			logger.error( "Exception is: ", e );
		}
		if ( filesImported > 0 ) {
			out.println( filesImported + " files were imported" );
			out.flush();
		}
		if ( filesNotImported > 0 ) {
			out.println( filesNotImported + " files were NOT imported" );
			out.flush();
		}
	}

	/**
	 * Process a data set directory
	 * 
	 * @param <E>
	 *            type of data set
	 * @param dsDirPath
	 *            data set directory path inside ZIP
	 * @param dsName
	 *            name of the data set type (for logging)
	 * @param type
	 *            data set type
	 * @param reader
	 *            reader for data set
	 * @param daoProvider
	 *            provider for the DAO
	 * @param rds
	 *            root data stock
	 * @param out
	 *            output writer
	 * @param digitFileProvider
	 *            digital file provider (may be <code>null</code> for non-{@link Source} data sets)
	 */
	private <E extends DataSet> void processDataSetDirectory( String dsDirPath, String dsName, DataSetType type, DataSetReader<E> reader, DaoProvider<E, ? extends DataSetDao<E, ?, ?>> daoProvider, RootDataStock rds, PrintWriter out, AbstractDigitalFileProvider digitFileProvider ) {
		TFile dsDirectory = new TFile( dsDirPath );
		if ( !dsDirectory.exists() || !dsDirectory.isDirectory() ) {
			if ( out != null ) {
				out.println( "Notice: the zip file does not contain any " + dsName + " data sets" );
				out.flush();
			}
		}
		else {
			if ( out != null ) {
				out.println( "processing " + dsName + " data sets" );
				out.flush();
			}

			this.importFiles( dsDirectory, type, reader, daoProvider, out, rds, digitFileProvider );
		}
	}


	/**
	 * Determine if a directory is a data set directory
	 * 
	 * @param directory
	 *            directory to test
	 * @return <code>true</code> if directory is a data set directory, <code>false</code> otherwise
	 */
	private boolean isDataSetDirectory(String directory) {
		return directory.equals(DatasetTypes.PROCESSES.getValue())
				|| directory.equals(DatasetTypes.LCIAMETHODS.getValue())
				|| directory.equals(DatasetTypes.FLOWS.getValue())
				|| directory.equals(DatasetTypes.FLOWPROPERTIES.getValue())
				|| directory.equals(DatasetTypes.UNITGROUPS.getValue())
				|| directory.equals(DatasetTypes.CONTACTS.getValue())
				|| directory.equals(DatasetTypes.SOURCES.getValue())
				|| directory.equals(DatasetTypes.LIFECYCLEMODELS.getValue());
	}

	/**
	 * Find the data sub directory
	 * 
	 * @param zipFile
	 *            the ZIP file
	 * @param zipFileName
	 *            the ZIP file name
	 * @return data sub directory
	 */
	@Deprecated // use findDataAbsDirectory instead 
	private String findDataSubDirectory( TFile zipFile, String zipFileName ) {
		String subDir = "";
		
		String[] topDirectories = zipFile.list();
		for ( String directory : topDirectories ) {
			if ( this.isDataSetDirectory( directory ) ) {
				// OK, the data sets seem to be in subdirectories directly under the zip root directory
				return subDir;
			}
			// haven't found data directories, let's scan one level deeper
			TFile zipSubDirectory = new TFile( zipFileName + "/" + directory );
			if ( !zipSubDirectory.isDirectory() ) {
				// no directory
				continue;
			}
			String[] secondLevelDirectories = zipSubDirectory.list();
			if ( secondLevelDirectories == null ) {
				// we have no subentries in this subdirectory
				continue;
			}
			for ( String secondDirectory : secondLevelDirectories ) {
				if ( this.isDataSetDirectory( secondDirectory ) ) {
					// OK, we found a possible data directory, let's use that
					subDir = "/" + directory;
					return subDir;
				}
			}

		}

		return subDir;
	}
	
	/**
	 * Find data directory
	 * 
	 * @param zipFile
	 *            the ZIP file
	 * @param depth
	 *            Maximum number of directories to traverse
	 * @return absolute path of directory that contains datasets
	 */
	private String findDataAbsDirectory(TFile zipFile, int depth) {
		if (depth < 1 || !zipFile.isDirectory())
			return ""; // bottom level reached

		String erd = "";
		for (String dir : zipFile.list()) {
			if (this.isDataSetDirectory(dir))
				return zipFile.getAbsolutePath(); // return full path
			erd = findDataAbsDirectory(new TFile(zipFile + "/" + dir), depth - 1);
			if (erd != "")
				return erd; // dir found, stop looking
		}
		return erd;
	}

}
