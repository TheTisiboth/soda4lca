package de.iai.ilcd.jmx;

import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.configuration.InitServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.management.ManagementFactory;

public class MBeanRegisterContextListener implements ServletContextListener {

	private ApplicationInfo applicationInfo;

	private static final Logger logger = LoggerFactory.getLogger(InitServletContextListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("Registering Mbeans...");
		initializeApplicationInfoObject();
		try {
			registerAppInfoToMBeanServer();
		} catch (Exception e) {
			logger.error("JMX MBeans could not be initialized!", e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	private void initializeApplicationInfoObject() {
		ConfigurationService configurationService = ConfigurationService.INSTANCE;
		applicationInfo = new ApplicationInfo(configurationService);
	}

	private void registerAppInfoToMBeanServer() throws Exception {
		MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
		ObjectName objectName = new ObjectName("de.iai.ilcd.jmx:name=applicationInfo");
		platformMBeanServer.registerMBean(applicationInfo, objectName);
	}
}
