package de.iai.ilcd.service;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.dao.JobMetaDataDao;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.security.User;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.job.JobListener;
import de.iai.ilcd.service.job.JobState;
import de.iai.ilcd.service.job.JobType;

/**
 * An abstract service class for jobs. This class puts jobs into job queue and
 * informs about updated job states and completion times.
 * 
 * @author sarai
 *
 */
public abstract class JobService {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	JobMetaDataDao jobMetaDataDao = new JobMetaDataDao();

	protected Scheduler scheduler;

	public JobService() throws SchedulerException {
		scheduler = StdSchedulerFactory.getDefaultScheduler();
		JobListener listener = new JobListener(this);
		scheduler.getListenerManager().addJobListener(listener);
	}

	/**
	 * Queues given job fired by user into job queue.
	 * 
	 * @param job
	 *            Job to be queued
	 * @param user
	 *            The user who fired job
	 * @throws PersistException
	 * @throws MergeException
	 * @throws JobNotScheduledException
	 */
	public void queueJob(JobDetail job, User user, JobType jobType)
			throws PersistException, MergeException, JobNotScheduledException {
		// Create job meta data with relevant information
		JobMetaData jobMetaData = this.createAndPersistJobMetaData(job, jobType, user);

		Trigger trigger = TriggerBuilder.newTrigger().startNow().build();

		// Add job to job queue
		try {
			log.debug("Trying to schedule job.");
			scheduler.scheduleJob(job, trigger);
			log.debug("scheduled job.");
		} catch (SchedulerException se) {
			log.warn("Could not schedule job!");
			if (log.isDebugEnabled())
				se.printStackTrace();
			jobMetaData.setJobState(JobState.ERROR);
			jobMetaDataDao.merge(jobMetaData);
			throw new JobNotScheduledException(se.toString());
		}

	}

	/**
	 * Creates new job metadata with given job, job type and user and persists
	 * newly created job.
	 * 
	 * @param job
	 *            The job of which job metadata shall be created
	 * @param jobType
	 *            The job type of given job
	 * @param user
	 *            The user who invoked job
	 * @return A JobMeataData object with initialized data
	 * @throws PersistException
	 */
	protected JobMetaData createAndPersistJobMetaData(JobDetail job, JobType jobType, User user)
			throws PersistException {
		JobMetaData jobMetaData = new JobMetaData();
		jobMetaData.setJobId(job.getKey().getName());
		jobMetaData.setJobName(job.getDescription());
		jobMetaData.setJobState(JobState.WAITING);
		jobMetaData.setJobType(jobType);
		jobMetaData.setLog("");
		jobMetaData.setUser(user);
		log.debug("Trying to persist job metadata.");
		this.jobMetaDataDao.persist(jobMetaData);
		return jobMetaData;
	}

	/**
	 * updates the JobMetaData entry with new given information about the
	 * currently executed job.
	 * 
	 * @param context
	 *            The job context containing the most of relevant information.
	 * @param jobCompletionTime
	 *            The date job has been completed
	 * @param jobState
	 *            The current job state
	 * @throws MergeException
	 */
	public void updateJob(JobExecutionContext context, Date jobCompletionTime, JobState jobState)
			throws MergeException {
		log.debug("updating job meta data");
		String jobId = context.getJobDetail().getKey().getName();
		JobMetaData jobMetaDataTmp = this.jobMetaDataDao.getJobMetaData(jobId);
		jobMetaDataTmp.setJobRunTime(context.getJobRunTime());
		jobMetaDataTmp.setJobCompletionTime(jobCompletionTime);
		jobMetaDataTmp.setJobFireTime(context.getFireTime());
		jobMetaDataTmp.setJobState(jobState);
		this.jobMetaDataDao.merge(jobMetaDataTmp);
	}

	/**
	 * Informs that the job with given description has not been executed
	 * successfully.
	 * 
	 * @param jobDescription
	 *            The description of job throwing an exception
	 */
	public void informError(String jobDescription) {

	}

	/**
	 * Informs that the job with given description was completed successfully.
	 * 
	 * @param jobDescription
	 *            The description of job that was completed successfully.
	 */
	public void informComplete(String jobDescription) {
		if (log.isInfoEnabled()) {
			log.info("Job with description \"" + jobDescription + "\" has successfully been completed.");
		}
	}
}
