package de.iai.ilcd.service.glad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.rest.util.InvalidGLADUrlException;
import de.iai.ilcd.service.JobService;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.job.GLADRegistrationJob;
import de.iai.ilcd.service.job.JobType;
import eu.europa.ec.jrc.lca.commons.dao.SearchParameters;
import eu.europa.ec.jrc.lca.commons.rest.dto.DataSetRegistrationResult;
import eu.europa.ec.jrc.lca.commons.service.exceptions.AuthenticationException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.NodeIllegalStatusException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.RestWSUnknownException;

/**
 * The implementation of the GLAD registration service object.
 * 
 * @author sarai
 *
 */
@Service("gladRegistrationService")
public class GLADRegistrationServiceImpl extends JobService implements GLADRegistrationService {

	private final Logger log = LoggerFactory.getLogger(GLADRegistrationServiceImpl.class);
	
	public static final int LARGE_WORKLOAD = 30; // If exceeded a background job is startet.

	@Autowired
	private GLADRegistrationDataDao dao;
	

	public GLADRegistrationServiceImpl() throws SchedulerException {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> getRegistered() {
		return dao.getRegistered();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DataSetRegistrationResult> register(List<Process> processes) throws RestWSUnknownException,
			AuthenticationException, NodeIllegalStatusException, InvalidGLADUrlException {
		List<DataSetRegistrationResult> resultList = new ArrayList<DataSetRegistrationResult>();

		if (processes.size() >= LARGE_WORKLOAD) {
			placeRegistrationOrder(processes);
			log.info("Issueing background registration job.");
		} else {
			for (Process process : processes) {
				DataSetRegistrationResult result = doRegister(process, dao);
				resultList.add(result);
			}
		}
		return resultList;
	}

	private void placeRegistrationOrder(List<Process> processes) {
		String jobDescription = "GLAD registration of " + processes.size() + " data sets.";

		JobDataMap data = new JobDataMap();
		data.put("dataSets", processes);
		data.put("daoInstance", dao);
		data.put("todo", "Registration");

		JobDetail job = JobBuilder.newJob(GLADRegistrationJob.class).withDescription(jobDescription).usingJobData(data).build();
		try {
			super.queueJob(job, null, JobType.OTHER);
		} catch (PersistException | MergeException | JobNotScheduledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void placeDeregistrationOrder(List<Process> processes) {
		String jobDescription = "GLAD deregistration of " + processes.size() + " data sets.";

		JobDataMap data = new JobDataMap();
		data.put("dataSets", processes);
		data.put("daoInstance", dao);
		data.put("todo", "Deregistration");

		JobDetail job = JobBuilder.newJob(GLADRegistrationJob.class).withDescription(jobDescription).usingJobData(data).build();
		try {
			super.queueJob(job, null, JobType.OTHER);
		} catch (PersistException | MergeException | JobNotScheduledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static DataSetRegistrationResult doRegister(DataSet dataSet, GLADRegistrationDataDao dao)
			throws NodeIllegalStatusException, AuthenticationException, RestWSUnknownException,
			InvalidGLADUrlException {
		DataSetRegistrationResult result;
		GLADRegistrationData grd = fetchGLADRegistrationData(dataSet, dao);

		RegistrationJobType jobType = determineJobType(dataSet, grd);
		if (jobType == RegistrationJobType.REJECTED) {
			result = DataSetRegistrationResult.REJECTED_NO_DIFFERENCE;
		} else {
			boolean isUpdate = (jobType == RegistrationJobType.UPDATE);
			result = GLADRestServiceBD.getInstance().registerDataSet(dataSet, isUpdate);

			if (result == DataSetRegistrationResult.ACCEPTED_PENDING)
				logRegistrationInformation(grd, dataSet, dao);
		}

		return result;
	}

	private static void logRegistrationInformation(GLADRegistrationData grd, DataSet dataSet,
			GLADRegistrationDataDao dao) {
		if (grd != null)
			dao.remove(grd);

		GLADRegistrationData grdNew = new GLADRegistrationData();
		grdNew.setUuid(dataSet.getUuidAsString());
		grdNew.setVersion(dataSet.getVersion());
		dao.saveOrUpdate(grdNew);
	}

	private static RegistrationJobType determineJobType(DataSet dataSet, GLADRegistrationData dataSetGLAD) {
		RegistrationJobType jobType;

		try {
			if (dataSetGLAD == null)
				jobType = RegistrationJobType.NEW;
			else if (dataSetGLAD.getVersion().compareTo(dataSet.getVersion()) >= 0)
				jobType = RegistrationJobType.REJECTED;
			else
				jobType = RegistrationJobType.UPDATE;

		} catch (Exception e) {
			jobType = RegistrationJobType.REJECTED;
		}

		return jobType;
	}

	private static GLADRegistrationData fetchGLADRegistrationData(DataSet dataSet, GLADRegistrationDataDao dao) {
		GLADRegistrationData grd;
		try {
			grd = dao.findByUUID(dataSet.getUuidAsString());
		} catch (NoResultException nre) {
			grd = null;
		}
		return grd;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deregister(List<Process> processes) {
		if(processes.size() >= LARGE_WORKLOAD) {
			this.placeDeregistrationOrder(processes);
		} else {
			for (Process process : processes) {
				try {
					doDeregister(process, dao);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void doDeregister(DataSet dataSet, GLADRegistrationDataDao dao) throws Exception {
		GLADRegistrationData regData = null;
		try {
			regData = dao.findByUUID(dataSet.getUuidAsString());
			if (regData != null) {
				GLADRestServiceBD.getInstance().deregisterDataSet(dataSet.getUuidAsString());
				dao.remove(regData.getId());
			}
		} catch (AuthenticationException ae) {
			ae.printStackTrace();
		} catch (RestWSUnknownException re) {
			try {
				if (regData != null) {
					GLADRestServiceBD.getInstance().deregisterDataSet(dataSet.getUuidAsString());
					dao.remove(regData.getId());
				}
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> getListOfRegistrations( Process process ) {
		return dao.getListOfRegistrations( process );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> loadLazy(SearchParameters sp) {
		return dao.find(sp);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long count(SearchParameters sp) {
		return dao.count(sp);
	}
	
	private enum RegistrationJobType {
		UPDATE, NEW, REJECTED;
	}

}
