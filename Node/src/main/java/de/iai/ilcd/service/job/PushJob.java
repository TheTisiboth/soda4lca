package de.iai.ilcd.service.job;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.FileParam;
import de.fzk.iai.ilcd.service.client.impl.ILCDClientResponse;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DigitalFile;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.common.PushConfig;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.JobMetaDataDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.model.source.Source;
import de.iai.ilcd.rest.util.NotAuthorizedException;
import de.iai.ilcd.service.PushService;
import de.iai.ilcd.util.sort.DataSetComparator;

/**
 * A job for pushing data sets.
 * 
 * @author sarai
 *
 */
public class PushJob implements Job {

	/*
	 * The push config of this push job
	 */
	public static PushConfig pushConfig;

	/*
	 * The Push Service that invoked this instance
	 */
	public static PushService pushService;

	/*
	 * The password
	 */
	String password;

	/*
	 * The unique ID of this job
	 */
	String jobId;

	/*
	 * The job metadata of this job
	 */
	JobMetaData jmd;
	
	/*
	 *The log of all successes in job execution 
	 */
	String successesLog;
	
	/*
	 * The log of all infos during job execution 
	 */
	String infosLog;
	
	/*
	 * The log of all errors during job execution 
	 */
	String errorsLog;
	
	/*
	 * The mapping from uuid and version of each pushed data set to flag whether pushed is true
	 */
	Map<String, Boolean> pushedDependencies = new HashMap<String, Boolean>();
	
	/*
	 * The mapping from uuid and version of each proccess to list of its dependencies 
	 */
	Map<String, List<String>> processDependencies = new HashMap<String, List<String>>();
	
	/*
	 * Mapping from uuid and version of each dependency to its data set information
	 */
	Map<String, DataSetInfo> dataSetInfos = new HashMap<String, DataSetInfo>();

	/*
	 * The logger for logging data
	 */
	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	/*
	 * The network client for network connection to target node
	 */
	protected static ILCDNetworkClient client = null;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			JobMetaDataDao jobMetaDataDao = new JobMetaDataDao();
			jmd = jobMetaDataDao.getJobMetaData(context.getJobDetail().getKey().getName());
			jmd.setErrorsCount(new Long(0));
			jmd.setInfosCount(new Long(0));
			jmd.setSuccessesCount(new Long(0));
			
			successesLog = "SUCCESS\n";
			infosLog = "INFO\n";
			errorsLog = "ERROR\n";

			JobDataMap jobDataMap = context.getMergedJobDataMap();
			jobId = context.getJobDetail().getKey().getName();
			pushService = (PushService) jobDataMap.get("pushService");
			pushConfig = (PushConfig) jobDataMap.get("pushConfig");
			client = (ILCDNetworkClient) jobDataMap.get("client");
			pushDataStock(pushConfig, password);
		} catch (Exception e) {
			log.warn("Push job could not be executed correctly!");
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
			throw new JobExecutionException(e);
		}

	}

	/**
	 * Pushes data stock given by push configuration entry to stock in target
	 * node given by push configuration entry. Needs password of target node for
	 * authorization
	 * 
	 * @param pushConfig
	 *            The push configuration containing all relevant data for
	 *            pushing data sets
	 * @param password
	 *            The password for authorizing target node
	 * @throws ILCDServiceClientException
	 * @throws NotAuthorizedException
	 * @throws IOException
	 * @throws Exception
	 *             If the ILCDNetworkclient cannot be instantiated or ILCDClient
	 *             cannot be authorized
	 */
	protected void pushDataStock(PushConfig pushConfig, String password)
			throws ILCDServiceClientException, NotAuthorizedException, IOException {
		long start = 0;
		if (log.isDebugEnabled()) {
			start = System.currentTimeMillis();
		}
		DataSetDao<Process, ?, ?> processDsDao = new ProcessDao();

		List<DataSet> dataSetList = new ArrayList<DataSet>();

		dataSetList.addAll(processDsDao.get(pushConfig.getSource()));
		log.debug("set dataset list.");

		getAndPushDependencies(dataSetList, pushConfig);

		for (DataSet dataSet : dataSetList) {
			this.pushDataSet(dataSet, pushConfig, false);
		}
		if (log.isDebugEnabled()) {
			long stop = System.currentTimeMillis();
			long duration = stop - start;
			log.debug("Job took " + (duration / 3600000) + " hours, " + ((duration % 3600000) / 60000) + " minutes, "
					+ ((duration % 60000) / 1000) + " seconds and " + (duration % 1000) + " milliseconds to complete.");
		}
		if (!errorsLog.equals("ERROR\n"))
			jmd.appendLog(errorsLog);
		if (!successesLog.equals("SUCCESS\n"))
			jmd.appendLog(successesLog);
		if (!infosLog.equals("INFO\n"))
			jmd.appendLog(infosLog);
	}

	/**
	 * Gets all dependencies and pushes them to target stock.
	 * 
	 * @param dataSetList
	 *            The list of initial data sets that shall be pushed
	 * @param client
	 *            The client that transfers data.
	 * @param pushConfig
	 * @throws IOException
	 */
	protected void getAndPushDependencies(List<DataSet> dataSetList, PushConfig pushConfig) throws IOException {
		List<DataSet> dependencyList = new ArrayList<DataSet>();
		for (DataSet dataSet : dataSetList) {
			if (dataSet != null) {
				DataSetDao<?, ?, ?> dataSetDao = DataSetDao.getDao(dataSet);
				Set<DataSet> resultDependencies = dataSetDao.getDependencies(dataSet, pushConfig.getDependenciesMode());
				if (resultDependencies != null) {
					dependencyList.addAll(resultDependencies);
				}
				List<String> dependencies = new ArrayList<String>();
				for (DataSet resultDependency : resultDependencies) {
					String uuidVersion = resultDependency.getUuid().getUuid() + "," + resultDependency.getVersion().getVersionString();
					dependencies.add(uuidVersion);
					DataSetInfo dsInfo = new DataSetInfo();
					dsInfo.setName(resultDependency.getName().getValue());
					dsInfo.setUuid(resultDependency.getUuid().getUuid());
					dsInfo.setVersion(resultDependency.getVersion().getVersionString());
					dsInfo.setType(resultDependency.getDataSetType().getValue());
					dataSetInfos.put(uuidVersion, dsInfo);
				}
				processDependencies.put(dataSet.getUuid().getUuid()+ "," + dataSet.getVersion().getVersionString(), dependencies);
				
			}
		}
		dependencyList.removeAll(dataSetList);
		
		Collections.sort(dependencyList, new DataSetComparator());
		HashSet<DataSet> dependencySet = new HashSet<DataSet>(dependencyList);
		if (log.isDebugEnabled()) {
			log.debug("Size of dependency list is: " + dependencyList.size());
		}
		
		if (dependencyList != null) {
			if (log.isTraceEnabled()) {
				log.trace("Pushing dependencies.");
			}

			for (DataSet ds : dependencySet) {
				if (log.isDebugEnabled()) {
					log.debug("current data set is of type: " + ds.getDataSetType().getValue());
				}
				this.pushDataSet(ds, pushConfig, true);
				if (log.isDebugEnabled())
					log.debug("successfully pushed dependency with uuid " + ds.getUuidAsString());
			}
		}
	}

	/**
	 * Pushes data set to stock in target node given by push configuration.
	 * 
	 * @param dataSet
	 *            The data set that shall be pushed
	 * @param client
	 *            The client for network connection to target node
	 * @param pushConfig
	 *            The push configuration containing all relevant data for
	 *            pushing data set to target node
	 * @param isDependency
	 * 				The flag whether given data set is dependency of another process           
	 * @throws IOException
	 *             If content of data set cannot be converted into InputStream
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void pushDataSet(DataSet dataSet, PushConfig pushConfig, boolean isDependency) throws IOException {
		log.debug("Begin of push data set");
		
		Class clazs = null;
		clazs = dataSet.getDataSetType().getILCDClass();
		
		// Pushes data set with given content to given instance
		ILCDClientResponse response = null;
		boolean exists = client.existsDataSet(clazs, dataSet.getUuid().getUuid(),
				dataSet.getVersion().getVersionString());
		if (!exists) {
			InputStream is = null;
			is = IOUtils.toInputStream(dataSet.getXmlFile().getContent(), "UTF-8");
			if (log.isDebugEnabled()) {
				log.debug("Got input stream.");
			}
			boolean check = dataSet instanceof Source;
			if (check) {
				response = pushSource(dataSet, is, pushConfig);
			} else {
				response = client.putDataSetAsStream(clazs, is, pushConfig.getTarget().getTargetDsUuid());
			}
			String uuidVersion = dataSet.getUuid().getUuid() + "," + dataSet.getVersion().getVersionString();
			if (isDependency && pushedDependencies.get(uuidVersion) == null) {
				boolean isPushed = response.getStatus() == 200;
				pushedDependencies.put(uuidVersion, isPushed);
			}
		}
		if (!isDependency)
			setLog(exists, response, dataSet);

	}

	/**
	 * Pushes given source via InputStream in to stock in target node given by
	 * push configuration.
	 * 
	 * @param source
	 *            The source to be pushed
	 * @param in
	 *            The InputStream
	 * @throws FileNotFoundException
	 *             If given file created from source cannot be found
	 */
	public ILCDClientResponse pushSource(DataSet dataSet, InputStream in, PushConfig pushConfig)
			throws FileNotFoundException {
		Source source = (Source) dataSet;
		ILCDClientResponse response = null;
		log.debug("Begin of pushing source.");
		List<DigitalFile> files = null;
		files = source.getFilesAsList();

		List<FileParam> fileParams = new ArrayList<FileParam>();
		if (log.isDebugEnabled()) {
			log.debug("number of files is " + files.size());
		}
		for (DigitalFile file : files) {
			log.debug("checking each file");
			try {
				FileParam fileParam = new FileParam(file.getFileName(),
						new FileInputStream(new File(file.getAbsoluteFileName())));
				fileParams.add(fileParam);
			} catch (FileNotFoundException fnfe) {
				log.warn("File was not found: " + file.getFileName());
				if (log.isDebugEnabled()) {
					fnfe.printStackTrace();
				}
			}
		}
		response = client.putSourceDataSetAsStream(in, pushConfig.getTarget().getTargetDsUuid(), fileParams);
		return response;
	}

	/**
	 * Sets the log of {@link JobMetaData} with transfer information of given
	 * status and data set.
	 * 
	 * @param status
	 *            The status of job transfer
	 * @param dataSet
	 *            The data set that was to be pushed
	 */
	private void setLog(boolean exists, ILCDClientResponse response, DataSet dataSet) {
		LogLevel logLvl;
		String logLevel = "";
		String suffix = "";
		boolean successful = true;
		StringBuilder sBuilder = new StringBuilder(" Dependencies status is:\n");
		List<String> dependencies = processDependencies.get(dataSet.getUuid().getUUID() + "," + dataSet.getVersion().getVersionString());
		if (dependencies != null) {
			for (String dependency : dependencies) {
				if (pushedDependencies.get(dependency) != null) {
					String status;
					String depSuffix;
					log.debug("dependency is: " + dependency);
					boolean dependencySuccessful = pushedDependencies.get(dependency);
					DataSetInfo dsInfo = dataSetInfos.get(dependency);
					if (!dependencySuccessful) {
						successful = false;
						status = "ERROR ";
						depSuffix = "could not be transferred";
					} else {
						status = "SUCCESS";
						depSuffix = "was transferred successfully";
					}
					sBuilder.append("&nbsp;&nbsp;&nbsp;").append(status).append(dsInfo.getType()).append(" dataset ")
					.append(dsInfo.getName()).append(" ( uuid: ").append(dsInfo.getUuid()) .append(") ").append(depSuffix).append("\n");
				}
			}
		}
		
		if (!exists && response.getStatus() == 200) {
			if (successful) {
				logLvl = LogLevel.SUCCESS;
				logLevel = " SUCCESS ";
				suffix = "was successfully transferred including all dependencies.";
				jmd.addSuccessesCount();
			} else {
				logLvl = LogLevel.ERROR;
				logLevel = " ERROR ";
				suffix = new StringBuilder("was successfully transferred, but at least one dependency wasn't.").append(sBuilder.toString()).toString();
				jmd.addErrorsCount();
			}
		} else if (exists) {
			logLvl = LogLevel.INFO;
			logLevel = " INFO ";
			jmd.addInfosCount();
			suffix = "was not transferred since it already exists in target database.";
		} else {
			logLvl = LogLevel.ERROR;
			logLevel = " ERROR ";
			suffix = new StringBuilder("could be not transferred due to an error.").append(sBuilder.toString()).toString();
			jmd.addErrorsCount();
			if (log.isDebugEnabled()) {
				log.debug("response status: " + response.getStatus());
			}
		}

		Date date = new Date();
		StringBuilder sb = new StringBuilder(date.toString()).append(logLevel).append("Data set ")
				.append(dataSet.getName().getValue()).append(" (uuid " + dataSet.getUuid().getUuid()).append(" version ").append(dataSet.getVersion()).append(") ")
				.append(suffix);
		switch (logLvl) {
			case SUCCESS : 
				successesLog = appendLog(successesLog, sb.toString());
				break;
			case INFO	 :
				infosLog = appendLog(infosLog, sb.toString());
				break;
			case ERROR	 :
				errorsLog = appendLog(errorsLog, sb.toString());
				break;
		}
		
		if (log.isTraceEnabled()) {
			log.trace("errors count: " + jmd.getErrorsCount());
			log.trace("infos count: " + jmd.getInfosCount());
			log.trace("successes count: " + jmd.getSuccessesCount());
		}
	}
	
	/**
	 * Appends the second STring toAppend to the first String logString and adds a new line.
	 * @param logString The String on which another String shall be appended
	 * @param toAppend The String that shall be appended to first String
	 * @returns A String containing the first String followed by the second String and line break
	 */
	private String appendLog(String logString, String toAppend) {
		if (log.isTraceEnabled())
			log.trace("log is: " + logString);
		
		logString = logString.concat(toAppend);
		logString = logString.concat("\n");
		
		if (log.isTraceEnabled())
			log.trace("log is now: " + logString);
		
		return logString;
	} 
	
	private enum LogLevel {
		SUCCESS, ERROR, INFO
	}
	
	/**
	 * local class containing all information relevant for log in JobMetaData
	 */
	private class DataSetInfo {
		private String uuid;
		private String name;
		private String version;
		
		private String type;
		
		public String getUuid() {
			return uuid;
		}
		
		public void setUuid(String uuid) {
			this.uuid = uuid;
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public String getVersion() {
			return this.version;
		}
		
		public void setVersion(String version) {
			this.version = version;
		}
		
		public String getType() {
			return this.type;
		}
		
		public void setType(String type) {
			this.type = type;
		}
	}
}
