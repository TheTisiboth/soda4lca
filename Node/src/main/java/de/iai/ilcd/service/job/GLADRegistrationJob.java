package de.iai.ilcd.service.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.common.Uuid;
import de.iai.ilcd.model.dao.JobMetaDataDao;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.rest.util.InvalidGLADUrlException;
import de.iai.ilcd.service.glad.GLADRegistrationDataDao;
import de.iai.ilcd.service.glad.GLADRegistrationServiceImpl;
import de.iai.ilcd.service.util.RequestRateLimiter;
import eu.europa.ec.jrc.lca.commons.rest.dto.DataSetRegistrationResult;
import eu.europa.ec.jrc.lca.commons.service.exceptions.AuthenticationException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.NodeIllegalStatusException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.RestWSUnknownException;

/**
 * A wrapper for the registration and deregistration methods
 * of <code>GLADRegistrationServiceImpl.class</code>
 *
 */
public class GLADRegistrationJob implements Job {

	private enum LogLevel {
		ERROR, SUCCESS, INFO;
	}

	private static final int RATE_LIMIT = 200;

	private Log log = new Log();

	private static final String REGISTRATION_KEY = "Registration";

	private static final String DEREGISTRATION_KEY = "Deregistration";

	private static final String DAO_INSTANCE_KEY = "daoInstance";

	private static final String DATA_SETS_KEY = "dataSets";

	private static final String JOB_TYPE_KEY = "todo";

	JobMetaData jmd;

	private JobMetaDataDao jmdDao;

	private String jobType;

	private void init(JobExecutionContext context) {
		jmdDao = new JobMetaDataDao();
		jmd = jmdDao.getJobMetaData(context.getJobDetail().getKey().getName());
		jmd.setErrorsCount(new Long(0));
		jmd.setInfosCount(new Long(0));
		jmd.setSuccessesCount(new Long(0));
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		init(context);
		JobDataMap data = context.getMergedJobDataMap();

		@SuppressWarnings("unchecked")
		List<DataSet> dataSets = (List<DataSet>) data.get(DATA_SETS_KEY);
		GLADRegistrationDataDao dao = (GLADRegistrationDataDao) data.get(DAO_INSTANCE_KEY);
		jobType = (String) data.get(JOB_TYPE_KEY);

		if (StringUtils.equals(jobType, REGISTRATION_KEY))
			register(dataSets, dao);
		else if (StringUtils.equals(jobType, DEREGISTRATION_KEY))
			deregister(dataSets, dao);

	}

	private void deregister(List<DataSet> dataSets, GLADRegistrationDataDao dao) {
		RequestRateLimiter rateLimiter = new RequestRateLimiter(RATE_LIMIT);
		for (DataSet ds : dataSets) {
			try {
				rateLimiter.check();
				GLADRegistrationServiceImpl.doDeregister(ds, dao);
				log.add(ds, DataSetRegistrationResult.ACCEPTED_PENDING);
			} catch (Exception e) {
				log.add(ds, DataSetRegistrationResult.ERROR);
			}
			updateJobMetaData();
		}
	}

	private void register(List<DataSet> dataSets, GLADRegistrationDataDao dao) {
		RequestRateLimiter rateLimiter = new RequestRateLimiter(RATE_LIMIT);
		for (DataSet ds : dataSets) {
			DataSetRegistrationResult result;
			try {
				rateLimiter.check();
				result = GLADRegistrationServiceImpl.doRegister(ds, dao);
			} catch (NodeIllegalStatusException | AuthenticationException | RestWSUnknownException
					| InvalidGLADUrlException e) {
				result = DataSetRegistrationResult.ERROR;
				e.printStackTrace();
			}
			log.add(ds, result);
			updateJobMetaData();
		}
	}

	private void updateJobMetaData() {
		jmd.setSuccessesCount(log.getSuccessesCount());
		jmd.setErrorsCount(log.getErrorsCount());
		jmd.setInfosCount(log.getInfosCount());
		jmd.setLog(log.print());
		try {
			jmdDao.merge(jmd);
		} catch (MergeException e) {
			e.printStackTrace();
		}
	}

	private class Log {

		private EnumMap<LogLevel, List<DataSetRegistrationResult>> logLevelMapper = new EnumMap<LogLevel, List<DataSetRegistrationResult>>(
				LogLevel.class);

		private EnumMap<DataSetRegistrationResult, List<Uuid>> preSortedLogs = new EnumMap<DataSetRegistrationResult, List<Uuid>>(
				DataSetRegistrationResult.class);

		private final String LINE_SEPARATOR = System.getProperty("line.separator");

		private long errorsCount = 0;

		private long successesCount = 0;

		private long infosCount = 0;

		/**
		 * Define which result gets mapped to which log level and initialises the
		 * pre-sorted logs.
		 */
		Log() {
			List<DataSetRegistrationResult> unmappedResults = new LinkedList<DataSetRegistrationResult>(
					Arrays.asList(DataSetRegistrationResult.values()));
			for (DataSetRegistrationResult key : unmappedResults) {
				preSortedLogs.put(key, new ArrayList<Uuid>());
			}

			// Errors
			List<DataSetRegistrationResult> codes = Stream.of(DataSetRegistrationResult.ERROR)
					.collect(Collectors.toList());
			logLevelMapper.put(LogLevel.ERROR, codes);
			unmappedResults.removeAll(codes);

			// Successes
			codes = Stream.of(DataSetRegistrationResult.ACCEPTED_PENDING).collect(Collectors.toList());
			logLevelMapper.put(LogLevel.SUCCESS, codes);
			unmappedResults.removeAll(codes);

			// Info
			logLevelMapper.put(LogLevel.INFO, unmappedResults);
		}

		void add(DataSet dataSet, DataSetRegistrationResult statusResult) {
			List<Uuid> entries = preSortedLogs.get(statusResult);
			entries.add(dataSet.getUuid());
			preSortedLogs.put(statusResult, entries);

			// TODO: wire to log level!
			if (statusResult.equals(DataSetRegistrationResult.ERROR))
				errorsCount++;
			else if (statusResult.equals(DataSetRegistrationResult.ACCEPTED_PENDING))
				successesCount++;
			else
				infosCount++;
		}

		String print() {
			StringBuilder sb = new StringBuilder();
			for (LogLevel key : logLevelMapper.keySet()) {
				sb.append(printLevelLog(key));
			}
			return sb.toString();
		}

		private String printLevelLog(LogLevel level) {
			StringBuilder sb = new StringBuilder();
			sb.append(addHeader(level));

			List<String> preLogs = logLevelMapper.get(level).stream().map(r -> printPreSortedLog(r))
					.collect(Collectors.toList());
			for (String s : preLogs)
				sb.append(s);
			sb.append(LINE_SEPARATOR);
			String levelLog = sb.toString();
			return StringUtils.equals(levelLog, addHeader(level) + LINE_SEPARATOR) ? "" : levelLog;
		}

		private String printPreSortedLog(DataSetRegistrationResult key) {
			StringBuilder sb = new StringBuilder("");
			List<String> ids = preSortedLogs.get(key).stream().map(uuid -> key + ": " + uuid + LINE_SEPARATOR)
					.collect(Collectors.toList());
			for (String s : ids)
				sb.append(s);
			return sb.toString();
		}

		private String addHeader(LogLevel level) {
			StringBuilder sb = new StringBuilder();
			sb.append(LINE_SEPARATOR);
			sb.append("---------");
			sb.append(level);
			sb.append("---------");
			sb.append(LINE_SEPARATOR);
			return sb.toString();
		}

		public long getErrorsCount() {
			return errorsCount;
		}

		public long getSuccessesCount() {
			return successesCount;
		}

		public long getInfosCount() {
			return infosCount;
		}

	}
}
