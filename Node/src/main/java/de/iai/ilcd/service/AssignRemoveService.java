package de.iai.ilcd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.security.User;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.exception.StockBusyException;
import de.iai.ilcd.service.job.AssignRemoveJob;
import de.iai.ilcd.service.job.DataSetsTypes;
import de.iai.ilcd.service.job.JobState;
import de.iai.ilcd.service.job.JobType;
import de.iai.ilcd.webgui.controller.admin.StockHandler;
import de.iai.ilcd.webgui.controller.admin.StockListHandler;

/**
 * The service class for assigning and removing data stocks.
 * 
 * @author alhajras
 *
 */
@Service("assignRemoveService")
public class AssignRemoveService extends JobService {

	public AssignRemoveService() throws SchedulerException {
		super();
	}

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());


	/**
	 * Assign-Remove data sets to-from data stock. It takes a list of data set types
	 * and a list of stocks from which to fetch all data sets of those selected data
	 * set types.
	 * 
	 * @param stockListHandler     the stocks we restrict our selection to.
	 * @param selectedDataSetTypes
	 * @param stockHandler         for the stock to/from which the data sets should
	 *                             be assigned/removed.
	 * @param user
	 * @param jobDescription
	 * @param jobType
	 * @throws SchedulerException       If scheduler cannot be instantiated or job
	 *                                  is not scheduled correctly
	 * @throws StockBusyException
	 * @throws PersistException
	 * @throws MergeException
	 * @throws JobNotScheduledException
	 */
	public void attachDetachDataStock(StockListHandler stockListHandler, ArrayList<DataSetsTypes> selectedDataSetTypes,
			StockHandler stockHandler, User user, String jobDescription, JobType jobType)
			throws SchedulerException, StockBusyException, PersistException, MergeException, JobNotScheduledException {

		DataSetsTypes[] dataSetsTypesArray = new DataSetsTypes[selectedDataSetTypes.size()];
		selectedDataSetTypes.toArray(dataSetsTypesArray);
		String stockUuid = stockHandler.getEntry().getUuid().getUuid().toString();

		if (stockIsBusy(stockUuid)) {
			throw new StockBusyException("The data stock ( " + stockHandler.getEntry().getName() + " ) is busy!");
		}

		JobDataMap jobDataMap = new JobDataMap(); 	// Configures the job by storing whatever the job needs in an
													// specialized (String, Object)-map. Look up the AssignRemoveJob
													// class to find out which keys are necessary and how they are used.
		log.debug("Getting Job description");
		jobDataMap.put("stockHandler", stockHandler);
		jobDataMap.put("stockUuid", stockUuid);
		jobDataMap.put("stockListHandler", stockListHandler);
		jobDataMap.put("selectedDataSetTypes", dataSetsTypesArray);
		jobDataMap.put("type", jobType);
		jobDataMap.put("singleDatasetType", false);
		jobDataMap.put("usingGlobalReferences", false);

		JobDetail job = JobBuilder.newJob(AssignRemoveJob.class).withIdentity(UUID.randomUUID().toString()).withDescription(jobDescription.toString())
				.usingJobData(jobDataMap).build();
		log.debug("Trying to queue detach job.");
		super.queueJob(job, user, jobType);
	}

	/**
	 * Checks if the data stock is busy in doing a job.
	 * 
	 * @param stockUuid
	 */
	private boolean stockIsBusy(String stockUuid) throws SchedulerException {

		for (JobExecutionContext activeJobs : super.scheduler.getCurrentlyExecutingJobs()) {
			String jobStockUuid = (String) activeJobs.getJobDetail().getJobDataMap().get("stockUuid");
			if (jobStockUuid != null && jobStockUuid.equals(stockUuid)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateJob(JobExecutionContext context, Date jobCompletionTime, JobState jobState)
			throws MergeException {
		log.debug("updating job meta data");
		String jobId = context.getJobDetail().getKey().getName();
		JobMetaData jobMetaDataTmp = this.jobMetaDataDao.getJobMetaData(jobId);
		jobMetaDataTmp.setJobRunTime(context.getJobRunTime());
		jobMetaDataTmp.setJobCompletionTime(jobCompletionTime);
		jobMetaDataTmp.setJobFireTime(context.getFireTime());
		jobMetaDataTmp.setJobState(jobState);
		jobMetaDataTmp.setJobName(jobMetaDataTmp.getJobName());
		this.jobMetaDataDao.merge(jobMetaDataTmp);
	}

	/**
	 * Creates a Job that assigns/removes data sets (of a given array) to a stock.
	 * 
	 * @param dataSets
	 * @param stockHandler         for the stock to/from which the data sets should
	 *                             be assigned/removed.
	 * @param user
	 * @param jobDescription
	 * @param jobType:           JobType.Attach or JobType.Detach data sets.
	 * 
	 * @throws StockBusyException
	 * @throws SchedulerException
	 * @throws PersistException
	 * @throws MergeException
	 * @throws JobNotScheduledException
	 */
	public void createBatchJob(DataSet[] dataSets, StockHandler stockHandler, User user, String jobDescription, JobType jobType)
			throws StockBusyException, SchedulerException, PersistException, MergeException, JobNotScheduledException {

		String stockUuid = stockHandler.getEntry().getUuid().getUuid().toString();

		if (stockIsBusy(stockUuid)) {
			throw new StockBusyException("The data stock ( " + stockHandler.getEntry().getName() + " ) is busy!");
		}

		JobDataMap jobDataMap = new JobDataMap(); 	// Configures the job by storing whatever the job needs in an
													// specialized (String, Object)-map. Look up the AssignRemoveJob
													// class to find out which keys are necessary and how they are used.
		log.debug("Getting Job description");
		jobDataMap.put("stockHandler", stockHandler);
		jobDataMap.put("stockUuid", stockUuid);
		jobDataMap.put("dataSetSelectableDataModel", dataSets);
		jobDataMap.put("type", jobType);
		jobDataMap.put("singleDatasetType", true);
		jobDataMap.put("usingGlobalReferences", false);

		JobDetail job = JobBuilder.newJob(AssignRemoveJob.class).withIdentity(UUID.randomUUID().toString()).withDescription(jobDescription)
				.usingJobData(jobDataMap).build();
		log.debug("Trying to queue attach job.");
		super.queueJob(job, user, jobType);

	}
	
	/**
	 * Creates a Job that takes a set of references and a set of stocks from which
	 * the referenced data sets are fetched. To consider all stocks one can just
	 * pass null for stocksToFetchFrom.
	 * 
	 * @param references
	 * @param stocksToFetchFrom:	the stocks we restrict our selection to.
	 * 								<code>null<code> if we want to fetch from all stocks efficiently..
	 * 
	 * @param stockHandler         for the stock to/from which the data sets should
	 *                             be assigned/removed.
	 * 
	 * @param user
	 * @param jobDescription
	 * @param jobType:           JobType.Attach or JobType.Detach data sets.
	 * 
	 * @throws StockBusyException
	 * @throws SchedulerException
	 * @throws PersistException
	 * @throws MergeException
	 * @throws JobNotScheduledException
	 */
	public void createBatchJobFromGlobalReferences(Set<GlobalReference> references,
			AbstractDataStock[] stocksToFetchFrom, StockHandler stockHandler, User user, String jobDescription,
			JobType jobType)
			throws StockBusyException, SchedulerException, PersistException, MergeException, JobNotScheduledException {

		String stockUuid = stockHandler.getEntry().getUuid().getUuid().toString();

		if (stockIsBusy(stockUuid)) {
			throw new StockBusyException("The data stock ( " + stockHandler.getEntry().getName() + " ) is busy!");
		}

		JobDataMap jobDataMap = new JobDataMap(); 	// Configures the job by storing whatever the job needs in an
													// specialized (String, Object)-map. Look up the AssignRemoveJob
													// class to find out which keys are necessary and how they are used.
		log.debug("Getting Job description");
		jobDataMap.put("stockHandler", stockHandler);
		jobDataMap.put("stockUuid", stockUuid);
		jobDataMap.put("references", references);
		jobDataMap.put("stocksToFetchFrom", stocksToFetchFrom);
		jobDataMap.put("type", jobType);
		jobDataMap.put("singleDatasetType", false);
		jobDataMap.put("usingGlobalReferences", true);

		JobDetail job = JobBuilder.newJob(AssignRemoveJob.class).withIdentity(UUID.randomUUID().toString())
				.withDescription(jobDescription).usingJobData(jobDataMap).build();
		log.debug("Trying to queue attach job.");
		super.queueJob(job, user, jobType);
	}
}
