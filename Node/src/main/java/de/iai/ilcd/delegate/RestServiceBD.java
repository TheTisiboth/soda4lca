package de.iai.ilcd.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;

import de.iai.ilcd.xml.read.JAXBContextResolver;

public abstract class RestServiceBD {
	
	private final Logger logger = LoggerFactory.getLogger( RestServiceBD.class );

	protected Client client;

	protected String restServletUrl;

	protected RestServiceBD( String serviceBasicUrl ) {
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add( JAXBContextResolver.class );
		cc.getProperties().put( ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true );
		cc.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create( cc );
		if (logger.isDebugEnabled())
			client.addFilter(new LoggingFilter());
		this.restServletUrl = serviceBasicUrl;
	}

	protected WebResource getResource( String servicePath ) {
		return client.resource( restServletUrl + servicePath );
	}

	protected ClientResponse getResponse( String servicePath ) {
		return getResource( servicePath ).get( ClientResponse.class );
	}
}
