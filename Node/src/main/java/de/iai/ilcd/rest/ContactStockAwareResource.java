package de.iai.ilcd.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of contact resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/contacts" )
public class ContactStockAwareResource extends ContactResource {

	@PUT
	@Path("{datasetId}")
	@Produces( "application/xml, text/xml" )
	public Response assignDataset(@PathParam("stockIdentifier") String stockIdentifier, @PathParam("datasetId") String datasetId, @QueryParam( "withDependencies" ) Integer dependencies, @QueryParam( "version" ) String version) {
		return assignDataSetToStock(stockIdentifier, datasetId, dependencies, version);
	}
	
	@DELETE
	@Path("{datasetId}")
	@Produces( "application/xml, text/xml" )
	public Response removeAssignedDataset(@PathParam("stockIdentifier") String stockIdentifier, @PathParam("datasetId") String datasetId, @QueryParam( "withDependencies" ) Integer dependencies, @QueryParam( "version" ) String version) {
		return removeDataSetFromStock(stockIdentifier, datasetId, dependencies, version);
	}
}
