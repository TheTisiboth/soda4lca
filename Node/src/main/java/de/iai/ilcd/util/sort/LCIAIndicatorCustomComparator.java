package de.iai.ilcd.util.sort;

import java.util.Comparator;

import org.apache.commons.collections.comparators.FixedOrderComparator;
import org.apache.commons.lang3.StringUtils;

import de.iai.ilcd.model.process.LciaResult;

public class LCIAIndicatorCustomComparator implements Comparator<LciaResult> {

	public static final String[] order = {"b2ad6d9a-c78d-11e6-9d9d-cec0c932ce01", "2105d3ac-c7c7-4c80-b202-7328c14c66e8", "0db6bc32-3f72-48b9-bdb3-617849c2752f", "6f1b7d2a-eb2d-4b86-9b4d-2301b3186400", "b5c629d6-def3-11e6-bf01-fe55135034f3", "2299222a-bbd8-474f-9d4f-4dd1f18aea7c", "3af763a5-b7a1-48c9-9cee-1f223481fcef", "503699e0-eca9-4089-8bf8-e0f49c93e578", "6c7a9d7d-4888-41ae-84bb-0d460ec52b80", "6453d675-cfd8-42c8-93e9-4ea34ab1b78e", "216596d1-9539-4237-80ff-5eef712b0a71", "b92648c3-e996-41ad-9dca-78c69932b779", "dea1149d-d918-4b48-a3af-838b0a1e4ca1", "b5c602c6-def3-11e6-bf01-fe55135034f3", "b5c632be-def3-11e6-bf01-fe55135034f3", "b5c610fe-def3-11e6-bf01-fe55135034f3", "b5c611c6-def3-11e6-bf01-fe55135034f3", "b5c614d2-def3-11e6-bf01-fe55135034f3", "b53ec18f-7377-4ad3-86eb-cc3f4f276b2b", "b5c619fa-def3-11e6-bf01-fe55135034f3", "ee1082d1-b0f7-43ca-a1f0-21e2a4a74511", "6800f5d3-7284-4ba3-8eb6-e3ab3ade7995", "9ee9886e-1430-4afe-ae1b-5580b76def07", "7fe353ed-2aac-4c81-9045-2f0ed4ef855e", "b2ad6890-c78d-11e6-9d9d-cec0c932ce01", "b2ad66ce-c78d-11e6-9d9d-cec0c932ce01", "b2ad6494-c78d-11e6-9d9d-cec0c932ce01", "b2ad6110-c78d-11e6-9d9d-cec0c932ce01", "77e416eb-a363-4258-a04e-171d843a6460","06dcd26f-025f-401a-a7c1-5e457eb54637","1e84a202-dae6-42aa-9e9d-71ea48b8be00","b4274add-93b7-4905-a5e4-2e878c4e4216","f58827d0-b407-4ec6-be75-8b69efb98a0f","f7c73bb9-ab1a-4249-9c6d-379a0de6f67e","804ebcdf-309d-4098-8ed8-fdaf2f389981"};

	private FixedOrderComparator comp = new FixedOrderComparator (order);
	
	@Override
	public int compare(LciaResult o1, LciaResult o2) {

		String uuid1 = o1.getMethodReference().getRefObjectId().trim().toLowerCase();
		String uuid2 = o2.getMethodReference().getRefObjectId().trim().toLowerCase();

		try {
			return comp.compare(uuid1, uuid2);
		} catch (IllegalArgumentException e) {
			return StringUtils.compare(uuid1, uuid2);
		}
	}
}
