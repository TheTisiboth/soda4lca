package de.iai.ilcd.util.sort;

import java.util.Comparator;

import de.iai.ilcd.model.tag.Tag;

public class TagComparator implements Comparator<Tag> {

	@Override
	public int compare(Tag tag1, Tag tag2) {
		return tag1.getName().compareTo(tag2.getName());
	}

}
