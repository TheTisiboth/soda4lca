package de.iai.ilcd.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.ValueParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.model.common.ClClass;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.persistence.PersistenceUtil;

/**
 * 
 * @author clemens.duepmeier
 */
public class ClassificationDao {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( ClassificationDao.class );

	/**
	 * Get a ClClass by clId
	 * 
	 * @param clId
	 *            the clId to get for
	 * @return loaded cl class
	 */
	public ClClass getByClId( String clId ) {
		EntityManager em = PersistenceUtil.getEntityManager();
		Query q = em.createQuery( "SELECT c FROM ClClass c WHERE c.clId=:clId", ClClass.class );
		q.setParameter( "clId", clId );

		@SuppressWarnings( "unchecked" )
		List<ClClass> results = q.getResultList();
		if ( results != null && results.get( 0 ) != null )
			return results.get( 0 );
		else
			return null;
	}

	/**
	 * Get a ClClass by clId
	 * 
	 * @param clId
	 *            the clId to get for
	 * @return loaded cl class
	 */
	public String getNameByClId( DataSetType datasetType, String clId ) {
		EntityManager em = PersistenceUtil.getEntityManager();
		Query q = em.createQuery( "SELECT DISTINCT c.name FROM ClClass c WHERE c.clId=:clId AND c.dataSetType=:dsType", ClClass.class );
		q.setParameter( "clId", clId );
		q.setParameter( "dsType", datasetType );

		@SuppressWarnings( "unchecked" )
		List<String> results = q.getResultList();
		if ( results != null && results.get( 0 ) != null )
			return results.get( 0 );
		else
			return null;
	}

	/**
	 * Get top classes for provided data stocks
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param classificationSystem
	 *            classification system
	 * @param stocks
	 *            data stocks
	 * @return top classes
	 */
	public List<ClClass> getTopClasses( DataSetType dataSetType, String classificationSystem, IDataStockMetaData... stocks ) {
		return getTopClasses(dataSetType, classificationSystem, null, stocks);
	}
	
	/**
	 * Get top classes for provided data stocks with additional parameters for process datasets
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param classificationSystem
	 *            classification system
	 * @param stocks
	 *            data stocks
	 * @return top classes
	 */
	@SuppressWarnings( "unchecked" )
	public List<ClClass> getTopClasses( DataSetType dataSetType, String classificationSystem, ValueParser params, IDataStockMetaData... stocks ) {
		EntityManager em = PersistenceUtil.getEntityManager();

		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<String> joins = new ArrayList<String>();
		List<String> wheres = new ArrayList<String>();

		joins.add( "LEFT JOIN a.classifications cl" );
		joins.add( "LEFT JOIN cl.classes clz" );

		wheres.add( "clz.level=0" );
		if ( StringUtils.isNotBlank( classificationSystem ) ) {
			wheres.add( "cl.name=:claName" );
			paramMap.put( "claName", classificationSystem );
		}

		// only for processes
		boolean hasDataSourceParam = (params!=null) && dataSetType.equals(DataSetType.PROCESS) && !StringUtils.isBlank(params.getString("dataSource"));

		if (hasDataSourceParam) {
			joins.add("LEFT JOIN a.dataSources datasource ");
			ProcessDao.addDatasourcesWhereClauses("a", params, wheres, paramMap);
		}

		this.addClassesStockClauses( joins, wheres, paramMap, stocks );

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append( "SELECT DISTINCT clz FROM " ).append( this.getEntityName( dataSetType ) ).append( " a " ).append( StringUtils.join( joins, " " ) );
		queryBuilder.append( " WHERE " );
		queryBuilder.append( StringUtils.join( wheres, " AND " ) );
		queryBuilder.append( " GROUP BY clz.name ORDER BY clz.name ASC" );

		Query q = em.createQuery( queryBuilder.toString(), ClClass.class );
		for ( Map.Entry<String, Object> param : paramMap.entrySet() ) {
			q.setParameter( param.getKey(), param.getValue() );
		}

		if ( LOGGER.isDebugEnabled() )
			LOGGER.debug( queryBuilder.toString() );

		return q.getResultList();

	}

	/**
	 * Get top classes for provided data stocks
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param stocks
	 *            data stocks
	 * @return top classes
	 */
	public List<ClClass> getTopClasses( DataSetType dataSetType, IDataStockMetaData... stocks ) {
		return this.getTopClasses( dataSetType, null, stocks );
	}

	/**
	 * Get the classification systems for provided top level class
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param topLevelClassName
	 *            name of top level class
	 * @return names of the classification systems
	 */
	@SuppressWarnings( "unchecked" )
	public List<String> getCategorySystemsOfTopLevelClass( DataSetType dataSetType, String topLevelClassName ) {
		EntityManager em = PersistenceUtil.getEntityManager();

		Query q = em.createQuery(
				"SELECT DISTINCT cla.name FROM Classification cla LEFT JOIN cla.classes clz WHERE clz.name=:clzName AND clz.dataSetType=:type ORDER BY cla.name ASC",
				String.class );

		q.setParameter( "type", dataSetType );
		q.setParameter( "clzName", topLevelClassName );

		return q.getResultList();
	}

	/**
	 * Get sub classes
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param classificationSystem
	 *            name of classification system
	 * @param classes
	 *            path of classes
	 * @param stocks
	 *            data stocks
	 * @return sub classes
	 */
	public List<ClClass> getSubClasses( DataSetType dataSetType, String classificationSystem, List<String> classes, boolean mostRecentVersionOnly,
			IDataStockMetaData... stocks ) {
		return getSubClasses(dataSetType, classificationSystem, classes, null, mostRecentVersionOnly, stocks);
	}
	
	/**
	 * Get sub classes
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param classificationSystem
	 *            name of classification system
	 * @param classes
	 *            path of classes
	 * @param params
	 *            additional parameters like dataSource
	 * @param stocks
	 *            data stocks
	 * @return sub classes
	 */
	@SuppressWarnings( "unchecked" )
	public List<ClClass> getSubClasses( DataSetType dataSetType, String classificationSystem, List<String> classes, ValueParser params, boolean mostRecentVersionOnly,
			IDataStockMetaData... stocks ) {

		if ( CollectionUtils.isEmpty( classes ) ){
			throw new IllegalArgumentException("Empty classes path not allowed!");
		}

		EntityManager em = PersistenceUtil.getEntityManager();

		StringBuilder queryBuilder = new StringBuilder();
		List<String> wheres = new ArrayList<String>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		final int clSize = classes.size();

		List<String> joins = new ArrayList<String>();

		joins.add( "LEFT JOIN a.classifications cl" );

		// only for processes
		boolean hasDataSourceParam = (params!=null) && dataSetType.equals(DataSetType.PROCESS) && !StringUtils.isBlank(params.getString("dataSource"));

		if (hasDataSourceParam) {
			joins.add("LEFT JOIN a.dataSources datasource ");
			ProcessDao.addDatasourcesWhereClauses("a", params, wheres, paramMap);
		}
		
		this.addClassesStockClauses( joins, wheres, paramMap, stocks );

		for ( int level = 0; level <= clSize; level++ ) {
			final String alias = "clz" + Integer.toString( level );
			final String nameParam = "name" + Integer.toString( level );

			joins.add( " JOIN cl.classes " + alias );
			wheres.add( alias + ".level=" + Integer.toString( level ) );
			wheres.add( alias + ".dataSetType=:dsType" );
			if ( level < clSize ) {
				wheres.add( alias + ".name=:" + nameParam );
				paramMap.put( nameParam, classes.get( level ) );
			}
		}

		if (mostRecentVersionOnly) {
			wheres.add(DataSetDao.buildMostRecentVersionsOnlySubQuery("a", this.getEntityName( dataSetType ), null, StringUtils.join(joins, " "),
					wheres));
		}
		
		paramMap.put( "dsType", dataSetType );

		queryBuilder.append( "SELECT DISTINCT clz" + clSize + " FROM " ).append( this.getEntityName( dataSetType ) ).append( " a " ).append( StringUtils.join( joins, " " ) );
		queryBuilder.append( " WHERE " );
		queryBuilder.append( StringUtils.join( wheres, " AND " ) );

		queryBuilder.append( " GROUP BY clz" + clSize + ".name " );

		Query q = em.createQuery( queryBuilder.toString(), ClClass.class );
		for ( Map.Entry<String, Object> param : paramMap.entrySet() ) {
			q.setParameter( param.getKey(), param.getValue() );
		}

		if ( LOGGER.isDebugEnabled() )
			LOGGER.debug( queryBuilder.toString() );

		return q.getResultList();
	}
	
	/**
	 * Add stock classes for classes queries. Prerequisites:
	 * <ul>
	 * <li>alias for the data set object must be <code>a</code></li>
	 * <li>alias <code>ads</code> must be unused</li>
	 * <li>and parameter name <code>rootDsId</code> must be unused</li>
	 * </ul>
	 * 
	 * @param joins
	 *            list of joins
	 * @param wheres
	 *            list of where clauses
	 * @param paramMap
	 *            parameter map
	 * @param stocks
	 *            data stock meta data
	 * @see #getSubClasses(DataSetType, String, List, IDataStockMetaData...)
	 * @see #getTopClasses(DataSetType, String, IDataStockMetaData...)
	 */
	private void addClassesStockClauses( List<String> joins, List<String> wheres, Map<String, Object> paramMap, IDataStockMetaData... stocks ) {
		if ( stocks != null ) {
			List<String> stockClauses = new ArrayList<String>();
			List<String> rootStockClauses = new ArrayList<String>();
			List<Long> dsIds = new ArrayList<Long>();
			boolean dsJoinDone = false;
			for ( IDataStockMetaData m : stocks ) {
				// root data stock
				if ( m.isRoot() ) {
					String paramName = "rootDsId" + Long.toString( m.getId() );
					rootStockClauses.add( "a.rootDataStock.id=:" + paramName );
					paramMap.put( paramName, m.getId() );
				}
				// non root data stock
				else {
					if ( !dsJoinDone ) {
						joins.add( "LEFT JOIN a.containingDataStocks ads" );
						dsJoinDone = true;
					}
					dsIds.add( m.getId() );
				}
			}
			if ( !rootStockClauses.isEmpty() ) {
				stockClauses.add( "(" + StringUtils.join( rootStockClauses, " OR " ) + ")" );
			}
			if ( dsJoinDone && !dsIds.isEmpty() ) {
				stockClauses.add( "ads.id IN(" + StringUtils.join( dsIds, ',' ) + ")" );
			}
			if ( !stockClauses.isEmpty() ) {
				wheres.add( "(" + StringUtils.join( stockClauses, " OR " ) + ")" );
			}
		}
	}

	/**
	 * Get sub classes
	 * 
	 * @param dataSetType
	 *            type of data set
	 * @param classificationSystem
	 *            name of classification system
	 * @param classname
	 *            name of class
	 * @param level
	 *            level of class
	 * @param stocks
	 *            data stocks
	 * @return sub classes
	 */
	@SuppressWarnings( "unchecked" )
	public List<ClClass> getSubClasses( DataSetType dataSetType, String classificationSystem, String classname, int level, boolean mostRecentVersionOnly,
			IDataStockMetaData... stocks ) {

		EntityManager em = PersistenceUtil.getEntityManager();

		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<String> joins = new ArrayList<String>();
		List<String> wheres = new ArrayList<String>();

		joins.add( "LEFT JOIN a.classifications cl" );
		joins.add( "JOIN cl.classes clz0" );
		joins.add( "JOIN cl.classes clz1" );

		this.addClassesStockClauses( joins, wheres, paramMap, stocks );

		wheres.add( "cl.name=:cName" );
		wheres.add( "clz0.level=:lvl" );
		wheres.add( "clz1.name=:clzName" );

		if (mostRecentVersionOnly) {
			wheres.add(DataSetDao.buildMostRecentVersionsOnlySubQuery("a", this.getEntityName( dataSetType ), null, StringUtils.join(joins, " "),
					wheres));
		}

		paramMap.put( "lvl", level );
		paramMap.put( "cName", classificationSystem );
		paramMap.put( "clzName", classname );

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append( "SELECT DISTINCT clz0 FROM " ).append( this.getEntityName( dataSetType ) ).append( " a " ).append( StringUtils.join( joins, " " ) );
		queryBuilder.append( " WHERE " );
		queryBuilder.append( StringUtils.join( wheres, " AND " ) );
		queryBuilder.append( " GROUP BY clz0.name" );

		Query q = em.createQuery( queryBuilder.toString(), ClClass.class );
		for ( Map.Entry<String, Object> param : paramMap.entrySet() ) {
			q.setParameter( param.getKey(), param.getValue() );
		}

		if ( LOGGER.isDebugEnabled() )
			LOGGER.debug( queryBuilder.toString() );

		return q.getResultList();
	}

	/**
	 * Get the names of all category systems (classifications)
	 * 
	 * @return names of all category systems (classifications)
	 */
	@SuppressWarnings( "unchecked" )
	public List<String> getCategorySystemNames() {
		EntityManager em = PersistenceUtil.getEntityManager();
		Query q = em.createQuery( "SELECT DISTINCT c.name FROM Classification c", String.class );
		return q.getResultList();
	}

	/**
	 * Get entity name from data set type
	 * 
	 * @param type
	 *            input data set type
	 * @return name of entity for queries
	 */
	private String getEntityName( DataSetType type ) {
		if ( DataSetType.PROCESS.equals( type ) ) {
			return "Process";
		}
		if ( DataSetType.FLOW.equals( type ) ) {
			return "Flow";
		}
		if ( DataSetType.FLOWPROPERTY.equals( type ) ) {
			return "FlowProperty";
		}
		if ( DataSetType.CONTACT.equals( type ) ) {
			return "Contact";
		}
		if ( DataSetType.LCIAMETHOD.equals( type ) ) {
			return "LCIAMethod";
		}
		if ( DataSetType.SOURCE.equals( type ) ) {
			return "Source";
		}
		if ( DataSetType.UNITGROUP.equals( type ) ) {
			return "UnitGroup";
		}
		return null;
	}
}
