package de.iai.ilcd.model.utils;

import org.apache.commons.lang3.StringUtils;

import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetVersion;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.dao.ElementaryFlowDao;
import de.iai.ilcd.model.dao.ProductFlowDao;
import de.iai.ilcd.model.flow.Flow;

public class LookupUtil {

	public DataSet lookup(GlobalReference ref) {
		if (ref.getType().equals(GlobalReferenceTypeValue.FLOW_DATA_SET))
			return lookupFlow(ref.getRefObjectId(), ref.getVersion());
		else
			return null;
	}

	public Flow lookupFlow(GlobalReference ref) {
		return lookupFlow(ref.getRefObjectId(), ref.getVersion());
	}

	public Flow lookupFlow(String uuid, DataSetVersion version) {
		// check for product
		ProductFlowDao pfDao = new ProductFlowDao();
		ElementaryFlowDao efDao = new ElementaryFlowDao();
		return lookupFlow(uuid, version, pfDao, efDao);
	}

	public Flow lookupFlow(String uuid, DataSetVersion version, ProductFlowDao pfDao, ElementaryFlowDao efDao) {
		Flow result = null;
		
		if (version != null && StringUtils.isNotBlank(version.getVersionString())) {
			result = pfDao.getByUuidAndVersion(uuid, version);
			if(result == null)
				result = efDao.getByUuidAndVersion(uuid, version);
		} else {
			result = pfDao.getByUuid(uuid);
			if(result == null)
				result = efDao.getByUuid(uuid);
		}
		return result;
	}
}
