package de.iai.ilcd.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.ClientHandlerException;

import de.fzk.iai.ilcd.service.client.FailedAuthenticationException;
import de.fzk.iai.ilcd.service.client.FailedConnectionException;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.fzk.iai.ilcd.service.client.impl.vo.Result;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.nodes.NetworkNode;

public class ForeignDataSetsQueryThread<T extends IDataSetListVO> implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger( ForeignDataSetsQueryThread.class );

	private int id;
	private ForeignDataSetsQueryController<T> threadControl;
	private NetworkNode node = null;
	private String baseUrl;

	public ForeignDataSetsQueryThread( int id, ForeignDataSetsQueryController<T> threadControl, NetworkNode node, String baseUrl ) {

		this.id = id;
		this.threadControl = threadControl;
		this.node = node;
		this.baseUrl = baseUrl;
	}

	@Override
	public void run() {
		List<T> results = new ArrayList<T>();
		try {
			// Thread.sleep( 5000 ); // for testing only!
			
			ILCDNetworkClient ilcdClient = new ILCDNetworkClient( baseUrl );
			ilcdClient.setOrigin(ConfigurationService.INSTANCE.getNodeInfo().getBaseURL());

			if (logger.isDebugEnabled())
				logger.debug("querying remote node at " + baseUrl);
			
			Result<T> resultList = ilcdClient.query( threadControl.getSearchResultClassType(), threadControl.getParamMap() );

			logger.debug( "found {} results, {} on page 1", resultList.getTotalSize(), resultList.getDataSets().size() );

			for ( T result : resultList.getDataSets() ) {
				result.setSourceId( node.getNodeId() );
				result.setHref( baseUrl.concat("processes/").concat(result.getUuidAsString()).concat("?version=").concat(result.getDataSetVersion()) );
				results.add( result );
			}
		}
		catch ( FailedConnectionException ex ) {
			logger.error( "connection to {} failed", baseUrl, ex );
		}
		catch ( FailedAuthenticationException ex ) {
			logger.error( "authentication failed", ex );
		}
		catch ( ClientHandlerException ex ) {
			threadControl.logException( node, ex );
		}
		catch ( Exception ex ) {
			logger.error( "Error querying foreign node", ex );
		}

		threadControl.doNotify( this.id, results );
	}

	public int getId() {
		return id;
	}

	public void setId( int id ) {
		this.id = id;
	}

	public NetworkNode getNode() {
		return node;
	}

	public void setNode( NetworkNode node ) {
		this.node = node;
	}

}
