package de.iai.ilcd.security;

import org.apache.shiro.web.mgt.CookieRememberMeManager;

import eu.europa.ec.jrc.lca.commons.security.SecurityContext;
import eu.europa.ec.jrc.lca.commons.util.ApplicationContextHolder;

public class CustomRememberMeManager extends CookieRememberMeManager {

	public CustomRememberMeManager() {
		super();
		SecurityContext securityContext = ApplicationContextHolder.getApplicationContext().getBean("securityContext", SecurityContext.class);
		byte[] cipherKey = securityContext.getCipherKey();
		setCipherKey(cipherKey);
	}

}
