package de.iai.ilcd.security;

import java.io.IOException;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.configuration.ConfigurationService;
import io.jsonwebtoken.Jwts;

/**
 * The filter for bearer token-based authentication
 * @author sarai
 *
 */
public class JWTVerifyingFilter extends AuthenticatingFilter {
	
	Logger logger = LoggerFactory.getLogger( JWTVerifyingFilter.class );
	
    public static final String USER_ID = "userName";
    public static final String PASSWORD = "password";
	
	protected static final String AUTHORIZATION_HEADER = "Authorization";
	
	/**
	 * Initializing the Filter.
	 */
	public JWTVerifyingFilter() {
		setLoginUrl(DEFAULT_LOGIN_URL);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoginUrl(String loginUrl) {
		String previous = getLoginUrl();
		if (previous != null) {
			this.appliedPaths.remove(previous);
		}
		super.setLoginUrl(loginUrl);
		this.appliedPaths.put(getLoginUrl(),  null);
	}
 
	/**
	 * {@inheritDoc}
	 */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
    	logger.debug("servlet request: " + request.getCharacterEncoding());
    	
	    	boolean loggedIn = false;
	    	if (isLoginRequest(request, response) || isLoggedAttempt(request, response)) {
	    		loggedIn = executeLogin(request, response);
	    		if (!loggedIn) {
	    			accessDenied(response);
	    		}
	    		if (logger.isDebugEnabled()) {
	    		logger.debug("user is finally logged in: " + loggedIn);
	    		}
	    		return loggedIn;
	    	}
	    	
	    	if (!loggedIn) {
	    		logger.debug("user not authorized according to wrong bearer token.");
	    	}
	    	if (ConfigurationService.INSTANCE.isTokenOnly()) { 
	    		if (!loggedIn) {
	    			accessDenied(response);
	    		}
	    		return loggedIn;
	    	}
        return true;
    }
    
    private void accessDenied(ServletResponse response) throws IOException {
    	response.resetBuffer();
		response.getOutputStream().write("Permission denied.".getBytes());
		HttpServletResponse httpResponse = WebUtils.toHttp(response);
		httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
    
    

    /**
     * {@inheritDoc}
     */
	@Override
	protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
		logger.debug("Trying to create token.");
		
		if (isLoginRequest(request, response)) {
            String json = IOUtils.toString(request.getInputStream());

            if (json != null && !json.isEmpty()) {

                try (JsonReader jr = Json.createReader(new StringReader(json))) {
                    JsonObject object = jr.readObject();
                    String username = object.getString(USER_ID);
                    String password = object.getString(PASSWORD);
                    return new UsernamePasswordToken(username, password);
                }
            }
        }
		
		if (isLoggedAttempt(request, response)) {
			String token = getAuthzHeader(request);   
			if (token != null && token.startsWith("Bearer ")) {
				token = token.substring(token.indexOf(" "));
				try {
					return createToken(token);
				} catch (AuthenticationException ae) {
					//Act as unauthenticated user
				}
			}
		}
		
		return new UsernamePasswordToken();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
		HttpServletResponse httpResponse = WebUtils.toHttp(response);
		httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		
		return false;
	}
	
	/**
	 * Checks if request is a request for authorization.
	 * @param request The request to check whether it is an authorization request 
	 * @param response
	 * @return True if the request is a log attempt
	 */
	protected boolean isLoggedAttempt(ServletRequest request, ServletResponse response) {
		String authzHeader = getAuthzHeader(request);
		return authzHeader != null;
	}
	
	/**
	 * Gets the authorization header from given request.
	 * @param request The request containing the authorization header
	 * @return The authorization header of the given request
	 */
	protected String getAuthzHeader(ServletRequest request) {
		HttpServletRequest httpRequest = WebUtils.toHttp(request);
		return httpRequest.getHeader(AUTHORIZATION_HEADER);
		

	}
	
	/**
	 * Creates a bearer token.
	 * @param token The String containing information to create bearer token
	 * @return A bearer token containing relevant information from given token String
	 */
	public BearerToken createToken(String token) {
		try {
			String username = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary("secret"))
		                .parseClaimsJws(token).getBody().getSubject();
			String randomness = (String) Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary("secret")).parseClaimsJws(token).getBody().get("random");
			return new BearerToken(username, randomness);
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("following error occurred:" + e);
				e.printStackTrace();
			}
			throw new AuthenticationException();
		}
	}

}
