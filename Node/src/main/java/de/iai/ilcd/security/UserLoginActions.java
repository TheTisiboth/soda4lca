package de.iai.ilcd.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 * 
 * @author clemens.duepmeier
 */
public class UserLoginActions {

	public void login( String userName, String password ) {
		login(userName, password, false);
	}
	
	public void login( String userName, String password, boolean remembered ) {
		Subject currentUser = SecurityUtils.getSubject();
		// if (currentUser.isAuthenticated())
		// return;
		UsernamePasswordToken token = new UsernamePasswordToken( userName, password );
		if (remembered) {
			token.setRememberMe(true);
		}
		currentUser.login( token );
		token.clear();
	}

	public void logout() {
		Subject currentUser = SecurityUtils.getSubject();
		// IlcdSecurityRealm realm= new IlcdSecurityRealm();
		// realm.clearAuthorizationInfo(currentUser.getPrincipals());
		if ( currentUser.isAuthenticated() || currentUser.isRemembered()) {
			currentUser.logout();
		}
	}

	public boolean isLoggedIn() {
		Subject currentUser = SecurityUtils.getSubject();
		if ( currentUser.isAuthenticated() || currentUser.isRemembered()) {
			return true;
			
		}
		else {
			return false;
		}
	}
}
