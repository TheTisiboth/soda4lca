package de.iai.ilcd.security;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * The class resembling the bearer token for authentication.
 * @author sarai
 *
 */
public class BearerToken implements AuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3379300982758142191L;

	private String username;
	
	private String randomness;
	
	/**
	 * Initializing the bearer token with given information
	 * @param username The name of the user to create bearer token for
	 * @param randomness The randomness for token related to given user
	 */
	public BearerToken (String username, String randomness) {
		 this.username = username;
	     this.randomness = randomness;
	} 
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getPrincipal() {
		return username;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getCredentials() {
		return randomness;
	}

}
