package de.iai.ilcd.webgui.controller.admin;

import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;

import de.iai.ilcd.model.dao.UserDao;
import de.iai.ilcd.model.security.Organization;
import de.iai.ilcd.model.security.User;
import de.iai.ilcd.security.IlcdAuthorizationInfo;
import de.iai.ilcd.util.SodaUtil;
import de.iai.ilcd.webgui.controller.DirtyFlagBean;

/**
 * Handler for the user list in admin area
 */
@ViewScoped
@ManagedBean( name = "userListHandler" )
public class UserListHandler extends AbstractAdminOrgDependentListHandler<User, UserDao> {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 1884241468855139691L;
	
	protected final static Logger log = org.apache.log4j.Logger.getLogger(UserListHandler.class);

	/**
	 * Dirty flag bean
	 */
	@ManagedProperty( value = "#{dirty}" )
	private DirtyFlagBean dirty;

	/**
	 * Create the handler
	 */
	public UserListHandler() {
		super( new UserDao() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteSelected() {
		final User[] selectedItems = this.getSelectedItems();
		if ( selectedItems == null ) {
			return;
		}

		for ( User item : selectedItems ) {
			// Admin user not deletable
			if ( ObjectUtils.equals( item.getId(), SodaUtil.ADMIN_ID ) ) {
				continue;	// not selectable in facelet and not deletable (double check)
			}
			try {
				this.getDao().remove( item );
				this.addI18NFacesMessage( "facesMsg.removeSuccess", FacesMessage.SEVERITY_INFO, item.getUserName() );
			}
			catch ( Exception ex ) {
				this.addI18NFacesMessage( "facesMsg.removeError", FacesMessage.SEVERITY_ERROR, item.getUserName() );
				if (log.isDebugEnabled()) {
					ex.printStackTrace();
				}
			}
		}

		this.dirty.stockModified();
		IlcdAuthorizationInfo.permissionsChanged();

		this.clearSelection();
		this.reloadCount();
	}

	/**
	 * Set the selected users.
	 * <p>
	 * <b>Do not replace</b> by {@link AbstractAdminListHandler#setSelectedItems(Object[]) setSelectedItems} in Facelets
	 * (see it's documentation for the reason)
	 * </p>
	 * 
	 * @param selected
	 *            selected users
	 */
	public void setSelectedUsers( User[] selected ) {
		super.setSelectedItems( selected );
	}

	/**
	 * Get the selected users.
	 * <p>
	 * <b>Do not replace</b> by {@link AbstractAdminListHandler#getSelectedItems() getSelectedItems} in Facelets (see
	 * it's documentation for the reason)
	 * </p>
	 * 
	 * @return selected users
	 */
	public User[] getSelectedUsers() {
		return super.getSelectedItems();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected long loadElementCount( Organization o ) {
		return this.getDao().getUsersCount( o );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<User> lazyLoad( Organization o, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {
		return this.getDao().getUsers( o, first, pageSize );
	}

	/**
	 * Get the dirty flag bean
	 * 
	 * @return dirty flag bean
	 */
	public DirtyFlagBean getDirty() {
		return this.dirty;
	}

	/**
	 * Set the dirty flag bean
	 * 
	 * @param dirty
	 *            the dirty flag bean
	 */
	public void setDirty( DirtyFlagBean dirty ) {
		this.dirty = dirty;
	}

}
