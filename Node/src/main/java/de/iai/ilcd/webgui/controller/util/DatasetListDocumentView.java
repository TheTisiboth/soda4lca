package de.iai.ilcd.webgui.controller.util;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@ViewScoped
public class DatasetListDocumentView extends AbstractDocumentView implements Serializable {

	private static final long serialVersionUID = 5612516188877667998L;
	
	public static Logger logger = LoggerFactory.getLogger( DatasetListDocumentView.class );

}
