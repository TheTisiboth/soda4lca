package de.iai.ilcd.webgui.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.ObjectUtils;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.common.DataStockRestriction;
import de.iai.ilcd.model.dao.CommonDataStockDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.security.StockAccessRight;
import de.iai.ilcd.security.StockAccessRightDao;
import de.iai.ilcd.util.SodaUtil;
import de.iai.ilcd.webgui.controller.DirtyFlagBean;

/**
 * Admin handler for stock list
 */
@ManagedBean
@ViewScoped
public class StockListHandler extends AbstractAdminListHandler<AbstractDataStock> {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = -8296677018448858250L;

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( StockListHandler.class );

	private boolean showHidden = ConfigurationService.INSTANCE.getDisplayConfig().isShowHiddenDatastocksDefault();
	
	private DataStockRestriction restriction = DataStockRestriction.BOTH;
	
	private List<AbstractDataStock> viewCache;
	
	/**
	 * Dirty flag bean
	 */
	@ManagedProperty( value = "#{dirty}" )
	private DirtyFlagBean dirty;

	/**
	 * ProcessListHandler for handling registration
	 */
	@ManagedProperty( value = "#{processListHandler}" )
	private ProcessListHandler processListHandler;

	/**
	 * DAO for model access
	 */
	private final CommonDataStockDao dao = new CommonDataStockDao();

	/**
	 * invoked on manual reorder of rows, the new order will be persisted
	 * 
	 * @param event
	 */
	public void onRowReorder(ReorderEvent event) {
		
		if (LOGGER.isDebugEnabled())
			LOGGER.debug("moving " + event.getFromIndex() + " to " + event.getToIndex());
		
		AbstractDataStock moved = viewCache.remove(event.getFromIndex());
		viewCache.add(event.getToIndex(), moved);
		
		for (int i=0; i < viewCache.size(); i++) {
		    AbstractDataStock ads = viewCache.get(i);

			if (LOGGER.isDebugEnabled())
				LOGGER.debug(ads.getName());

			try {
				AbstractDataStock actualDataStock = dao.getById(ads.getId());
				actualDataStock.getDisplayProperties().setOrdinal(i);
				dao.persist(actualDataStock);
			} catch (Exception e) {
				LOGGER.error("error persisting datastocks' display order", e);
			}
		}
		
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected long loadElementCount() {
		Map<String, Object> filters = new HashMap<String, Object>();
		if (!this.showHidden) {
			filters.put("displayProperties.hidden", false);
		} else
			filters.put("displayProperties.hidden", true);
		
		switch (restriction) {
		case LOGICAL: filters.put("stockProperties.logical", true); 
						break;
		case ROOT: filters.put("stockProperties.logical", false);
					break;
		default:;
		}
		return this.dao.getAllCount(filters);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postConstruct() {
	}

	/**
	 * Set the selected stocks.
	 * <p>
	 * <b>Do not replace</b> by {@link AbstractAdminListHandler#setSelectedItems(Object[]) setSelectedItems} in Facelets
	 * (see it's documentation for the reason)
	 * </p>
	 * 
	 * @param selected
	 *            selected root data stocks
	 */
	public void setSelectedStocks( AbstractDataStock[] selected ) {
		super.setSelectedItems( selected );
	}

	/**
	 * Get the selected stocks.
	 * <p>
	 * <b>Do not replace</b> by {@link AbstractAdminListHandler#getSelectedItems() getSelectedItems} in Facelets (see
	 * it's documentation for the reason)
	 * </p>
	 * 
	 * @return selected root data stocks
	 */
	public AbstractDataStock[] getSelectedStocks() {
		return super.getSelectedItems();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteSelected() {
		final AbstractDataStock[] selected = this.getSelectedItems();
		if ( selected == null ) {
			return;
		}

		StockAccessRightDao sarDao = new StockAccessRightDao();
		for ( AbstractDataStock rds : selected ) {
			// Default root stock not deletable
			if ( ObjectUtils.equals( rds.getId(), SodaUtil.DEFAULT_ROOTSTOCK_ID ) ) {
				continue;	// not selectable in facelet and not deletable (double check)
			}
			List<StockAccessRight> sars = sarDao.get( rds );
			try {
				sarDao.remove( sars );
				this.dao.remove( rds );
				this.addI18NFacesMessage( "facesMsg.removeSuccess", FacesMessage.SEVERITY_INFO, rds.getLongTitle().getDefaultValue() + " (" + rds.getName()
						+ ")" );
			}
			catch ( Exception e ) {
				this.addI18NFacesMessage( "facesMsg.removeError", FacesMessage.SEVERITY_ERROR, rds.getLongTitle().getDefaultValue() + " (" + rds.getName()
						+ ")" );
			}
		}
		this.dirty.stockModified();
		this.clearSelection();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AbstractDataStock> lazyLoad( int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {
		if (!this.showHidden) {
			filters.put("displayProperties.hidden", false);
		} else
			filters.put("displayProperties.hidden", true);
		
		switch (restriction) {
		case LOGICAL: filters.put("stockProperties.logical", true); 
						break;
		case ROOT: filters.put("stockProperties.logical", false);
					break;
		default:;
		}
		
			
		 List<AbstractDataStock> list = this.dao.get( first, pageSize, sortField, sortOrder, filters );
		 
		 viewCache = list;
		 
		 return list;
	}

	public void registerSelected() {
		List<Process> selectedProcesses = new ArrayList<Process>();
		
		// select all newest processes in selected stock(s)
		ProcessDao pDao = new ProcessDao();
		long count = pDao.getCount(this.getSelectedStocks(), null, true);
		selectedProcesses.addAll(pDao.getDataSets(this.getSelectedStocks(), null, true, 0, (int) count));
		this.processListHandler.setSelectedItems(selectedProcesses.toArray(new Process[0]) );
		this.processListHandler.registerSelected();
	}
	/**
	 * Get the dirty flag bean
	 * 
	 * @return dirty flag bean
	 */
	public DirtyFlagBean getDirty() {
		return this.dirty;
	}

	/**
	 * Set the dirty flag bean
	 * 
	 * @param dirty
	 *            the dirty flag bean
	 */
	public void setDirty( DirtyFlagBean dirty ) {
		this.dirty = dirty;
	}

	public boolean isShowHidden() {
		return showHidden;
	}

	public void setShowHidden(boolean showHidden) {
		this.showHidden = showHidden;
	}


	public ProcessListHandler getProcessListHandler() {
		return processListHandler;
	}


	public void setProcessListHandler(ProcessListHandler processListHandler) {
		this.processListHandler = processListHandler;
	}
	
	public DataStockRestriction getDataStockRestriction() {
		return restriction;
	}
	
	public void setDataStockRestriction(DataStockRestriction restriction) {
		this.restriction = restriction;
	}


	public List<AbstractDataStock> getViewCache() {
		return viewCache;
	}

}
