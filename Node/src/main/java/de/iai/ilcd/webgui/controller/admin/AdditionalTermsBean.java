package de.iai.ilcd.webgui.controller.admin;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import de.iai.ilcd.model.common.AdditionalTerm;
import de.iai.ilcd.service.AdditionalTermsService;

/**
 * The bean for managing additional terms defined in config file.
 * 
 * @author sarai
 *
 */
@ManagedBean(name = "additionalTermsBean")
@ApplicationScoped
public class AdditionalTermsBean {

	@SuppressWarnings("unused")
	private Logger log = LoggerFactory.getLogger(this.getClass());

	AdditionalTermsService additionalTermsService;

	/**
	 * Initializes all used services.
	 */
	public AdditionalTermsBean() {
		WebApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
		this.additionalTermsService = ctx.getBean(AdditionalTermsService.class);
	}

	/**
	 * Gets all additional terms.
	 * 
	 * @return A list of all additional terms
	 */
	public List<AdditionalTerm> getAdditionalTerms() {
		return additionalTermsService.getAdditionalTerms();
	}

	/**
	 * Checks if any additional terms are defined in config file.
	 * 
	 * @return true if at leat one additional term is defined in config file
	 */
	public boolean isRenderAdditionalTerms() {
		return (additionalTermsService.getAdditionalTerms() != null)
				&& !additionalTermsService.getAdditionalTerms().isEmpty();
	}

}
