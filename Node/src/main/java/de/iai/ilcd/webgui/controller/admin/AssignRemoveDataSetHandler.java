package de.iai.ilcd.webgui.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.zip.DataFormatException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.primefaces.event.FileUploadEvent;
import org.quartz.SchedulerException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.dao.DependenciesMode;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.service.AssignRemoveService;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.exception.StockBusyException;
import de.iai.ilcd.service.exception.UserNotExistingException;
import de.iai.ilcd.service.job.DataSetsTypes;
import de.iai.ilcd.service.job.JobType;
import de.iai.ilcd.util.DataSetSelectableDataModel;
import de.iai.ilcd.util.DependenciesOptions;
import de.iai.ilcd.util.StockChildrenWrapper;
import de.iai.ilcd.webgui.controller.AbstractHandler;
import de.iai.ilcd.webgui.controller.util.BatchAssignMode;

/**
 * Handler to assign and remove entries.
 * 
 * @author alhajras
 *
 */
@ManagedBean
@ViewScoped
public class AssignRemoveDataSetHandler extends AbstractHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2499183803964247343L;

	/*
	 * The logger for logging data
	 */
	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	/*
	 * The mode of the assign/remove batch operation: all, none, secondary
	 */
	public String selectMode = "2";
	
	/*
	 * Shows the job redirecting button after batching
	 */
	public boolean showJobsButton = false;

	/*
	 * Service for scheduling push jobs
	 */
	private final AssignRemoveService assignRemoveService;
	
	@ManagedProperty("#{excelSelectionHandler}")
	private ExcelSelectionHandler excelSelectionHandler = new ExcelSelectionHandler();
	
	private Boolean isFileKnown = false;
	
	private Boolean fetchFromAllStocks = false;

	/*
	 * The decidable dependency options
	 */
	private DependenciesOptions depOptions = new DependenciesOptions();
	
	private BatchAssignMode selectionBy = BatchAssignMode.DATA_SET_TYPE;

	/*
	 * The handler for managing source stocks
	 */
	@ManagedProperty(value = "#{stockListHandler}")
	private StockListHandler stockListHandler = new StockListHandler();

	@ManagedProperty(value = "#{stockHandler}")
	private StockHandler stockHandler = new StockHandler();

	/*
	 * Flag for checking whether current view is editable
	 */
	private boolean edit = false;
	
	/*
	 * Checks whether batching the big datasets is needed
	 */
	private boolean batchNeeded = false;
		
	/*
	 * Checks if the user manually clicks on the create job button
	 */
	private boolean isForced = false;

	/*
	 * The number of datasets selected in from the table
	 */
	private long datasetsSelectedCount = 0;

	private List<DataSetsTypes> dataSets = new ArrayList<DataSetsTypes>();

	private ArrayList<DataSetsTypes> selectedDataSetTypes = new ArrayList<DataSetsTypes>();
	
	public AssignRemoveDataSetHandler() {
		super();
		WebApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
		this.assignRemoveService = ctx.getBean(AssignRemoveService.class);
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	/**
	 * Gets the dependencies option.
	 * 
	 * @return dependencies option for push configuration
	 */
	public DependenciesOptions getDependenciesOptions() {
		return this.depOptions;
	}

	public DependenciesMode getDepOption() {
		return this.depOptions.getDependenciesOption();
	}

	public void depOptionChange(ValueChangeEvent event) {
		if (event.getNewValue() != null)
			this.depOptions.setDependenciesOption((DependenciesMode) event.getNewValue());
	}

	/**
	 * Gets the current stock handler for managing available source stocks.
	 * 
	 * @return A StockHandler for managing available source stocks
	 */
	public StockListHandler getStockListHandler() {
		return stockListHandler;
	}

	/**
	 * Sets the StockHandler for managing source stocks.
	 * 
	 * @param stockListHandler The stock handler for managing source stocks
	 */
	public void setStockListHandler(StockListHandler stockListHandler) {
		this.stockListHandler = stockListHandler;
	}

	/**
	 * Detach Selected datasets from stock
	 */
	public void detachSelectedFromStock() {
		if (stockListHandler.getSelectedStocks().length == 0)
			return;
		if(selectionBy == BatchAssignMode.EXCEL_IMPORT) {
			attachDetachExcelSelectionFromStock(JobType.DETACH);
		} else {
			if(selectedDataSetTypes.isEmpty())
				return;
			
			try {
				assignRemoveService.attachDetachDataStock(stockListHandler, selectedDataSetTypes, stockHandler,
						stockHandler.getCurrentUser(), "Processing ...", JobType.DETACH);
				showJobsButton = true;
				this.addI18NFacesMessage("facesMsg.stock.createdNewJob", FacesMessage.SEVERITY_INFO);
			} catch (StockBusyException e) {
				this.addI18NFacesMessage("facesMsg.stock.isBusy", FacesMessage.SEVERITY_ERROR);
				log.error(e.toString());
			} catch (Exception e) {
				log.error(e.toString());
			}
		}
	}

	/**
	 * Attach Selected datasets to stock
	 */
	public void attachSelectedToStock() {
		if (stockListHandler.getSelectedStocks().length == 0)
			return;
		if(selectionBy == BatchAssignMode.EXCEL_IMPORT) {
			attachDetachExcelSelectionFromStock(JobType.ATTACH);
		} else {
			if(selectedDataSetTypes.isEmpty())
				return;
			
			try {
				assignRemoveService.attachDetachDataStock(stockListHandler, selectedDataSetTypes, stockHandler,
						stockHandler.getCurrentUser(), "Processing ...", JobType.ATTACH);
				showJobsButton = true;
				this.addI18NFacesMessage("facesMsg.stock.createdNewJob", FacesMessage.SEVERITY_INFO);
			} catch (StockBusyException e) {
				this.addI18NFacesMessage("facesMsg.stock.isBusy", FacesMessage.SEVERITY_ERROR);
				log.error(e.toString());
			} catch (Exception e) {
				log.error(e.toString());
			}
		}
	}
	
	public void handleExcelImport(FileUploadEvent event) {
		try {
			excelSelectionHandler.handleUpload(event);
			this.isFileKnown = true;
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			this.addI18NFacesMessage("admin.assignRemove.severeUploadError", e);
		} catch (DataFormatException e) {
			this.addI18NFacesMessage("facesMsg.import.fileuploadError2", e);
		}
	}
	
	public void attachDetachExcelSelectionFromStock(JobType jobType) {
		Set<GlobalReference> parsedReferences = excelSelectionHandler.getParsedReferences();
		if (parsedReferences.size() == 0) {
			this.addI18NFacesMessage("No data sets to attach/detach...", FacesMessage.SEVERITY_ERROR);
			return;
		}

			AbstractDataStock[] selectedSourceStocks = null;
		if( !fetchFromAllStocks ) {
			selectedSourceStocks = stockListHandler.getSelectedStocks();
		}
		try {
			assignRemoveService.createBatchJobFromGlobalReferences( parsedReferences, selectedSourceStocks, stockHandler, stockHandler.getCurrentUser(), "Processing...", jobType);
			showJobsButton = true;
			this.addI18NFacesMessage("facesMsg.stock.createdNewJob", FacesMessage.SEVERITY_INFO);
		} catch (StockBusyException e) {
			this.addI18NFacesMessage("facesMsg.stock.isBusy", FacesMessage.SEVERITY_ERROR);
			log.error(e.toString());
		} catch (Exception e) {
			log.error(e.toString());
		}
	}

	public List<DataSetsTypes> getDataSets() {
		dataSets = Arrays.asList(DataSetsTypes.values());
		return dataSets;
	}

	public void setDataSets(List<DataSetsTypes> dataSets) {
		this.dataSets = dataSets;
	}

	/**
	 * Checks if batch operation is needed for large datasets
	 */
	public void batchNeeded(DataSetSelectableDataModel<DataSet> ds, StockChildrenWrapper<DataSet> dsw) {
		if (ds != null || dsw != null) {
			depOptions = dsw.getDependenciesOptions();
			this.datasetsSelectedCount = ds.getSelected().length;
		}
	}
	
	/**
	 * Batching big datasets and create a job in the job list, only attach and detach are supported
	 */
	public void batch(DataSetSelectableDataModel<DataSet> ds, String action) {
		JobType jobType = JobType.ATTACH;
		if (action.contains("detach"))
			jobType = JobType.DETACH;
		try {
			assignRemoveService.createBatchJob(ds.getSelected(), stockHandler, stockHandler.getCurrentUser(),
					"Processing ...", jobType);
			this.addI18NFacesMessage("facesMsg.stock.createdNewJob", FacesMessage.SEVERITY_INFO);
		} catch (SchedulerException | PersistException | MergeException | JobNotScheduledException
				 | StockBusyException | UserNotExistingException e) {
			// TODO Auto-generated catch block
			this.addI18NFacesMessage("facesMsg.stock.isBusy", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}
	}

	public ArrayList<DataSetsTypes> getselectedDataSetTypes() {
		switch (this.selectMode) {
		case "1":
			selectedDataSetTypes.addAll(Arrays.asList(DataSetsTypes.values()));
			break;

		case "2":
			selectedDataSetTypes.clear();
			break;

		case "3":
			selectedDataSetTypes.clear();
			selectedDataSetTypes.addAll(Arrays.asList(DataSetsTypes.values()));
			selectedDataSetTypes.remove(DataSetsTypes.PROCESSES);
			selectedDataSetTypes.remove(DataSetsTypes.LCIA_METHODS);
			break;

		default:
			break;
		}
		return selectedDataSetTypes;
	}
	
	public void selectionByListener() {
		if (selectionBy == BatchAssignMode.DATA_SET_TYPE) {
			this.unselectAllStocks();
			fetchFromAllStocks = false;
		} else {
			this.selectAllVisibleStocks();
			fetchFromAllStocks = true;
		}
	}
	
	private void selectAllVisibleStocks() {
		this.stockListHandler.setSelectedStocks(
				this.stockListHandler.getViewCache().toArray(new AbstractDataStock[this.stockListHandler.getViewCache().size()]));
	}
	
	private void unselectAllStocks() {
		this.stockListHandler.clearSelection();
	}
	
	public void fetchFromAllStocksListener() {
		if (fetchFromAllStocks) {
			this.selectAllVisibleStocks();
		} else {
			this.unselectAllStocks();
		}
	}

	public void setselectedDataSetTypes(ArrayList<DataSetsTypes> selectedDataSetTypes) {
		this.selectedDataSetTypes = selectedDataSetTypes;
	}

	public String getSelectMode() {
		return selectMode;
	}

	public void setSelectMode(String selectMode) {
		this.selectMode = selectMode;
	}
	
	public StockHandler getStockHandler() {
		return stockHandler;
	}

	public void setStockHandler(StockHandler stockHandler) {
		this.stockHandler = stockHandler;
	}
	
	public boolean isShowJobsButton() {
		return showJobsButton;
	}

	public void setShowJobsButton(boolean showJobsButton) {
		this.showJobsButton = showJobsButton;
	}
	
	public boolean isBatchNeeded() {
		if (isForced)
			return this.batchNeeded;
		return datasetsSelectedCount >= 500;
	}
	
	public void resetForce() {
		isForced = false;
	}

	public void setBatchNeeded(boolean batchNeeded) {
		isForced = true;
		this.batchNeeded = batchNeeded;
	}

	public String getSelectionBy() {
		return selectionBy.getValue();
	}

	public void setSelectionBy(String selectionBy) {
		this.selectionBy = BatchAssignMode.fromValue(selectionBy);
	}

	public ExcelSelectionHandler getExcelSelectionHandler() {
		return excelSelectionHandler;
	}

	public void setExcelSelectionHandler(ExcelSelectionHandler excelSelectionHandler) {
		this.excelSelectionHandler = excelSelectionHandler;
	}

	public Boolean getIsFileKnown() {
		return isFileKnown;
	}

	public Boolean getFetchFromAllStocks() {
		return fetchFromAllStocks;
	}

	public void setFetchFromAllStocks(Boolean fetchFromAllStocks) {
		this.fetchFromAllStocks = fetchFromAllStocks;
	}
	
	public Boolean selectionIsByExcel() {
		return (selectionBy == BatchAssignMode.EXCEL_IMPORT);
	}
	
	public Boolean selectionIsByType() {
		return (selectionBy == BatchAssignMode.DATA_SET_TYPE);
	}
}
