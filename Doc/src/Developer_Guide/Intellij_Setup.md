# IntelliJ IDEA Setup


## Install & configure Tomcat

1. [Download Tomcat](https://tomcat.apache.org/) and extract it
2. Download the Mysql Connector from [here](https://dev.mysql.com/downloads/connector/j/) and extract into the `$TOMCAT/lib/` folder
3. Edit `$TOMCAT/conf/server.xml` according to [Eclipse Setup](/Doc/src/Developer_Guide/Eclipse_Setup.md)
4. Create a `$TOMCAT/conf/soda4lca.properties` file according to [Eclipse Setup](/Doc/src/Developer_Guide/Eclipse_Setup.md)
5. Edit `$TOMCAT/conf/context.xml` according to [Eclipse Setup](/Doc/src/Developer_Guide/Eclipse_Setup.md)


## Configure IntellJ

1. Checkout the repository
2. Open the Project (File -> Open...) and select the repository
3. IntelliJ imports the maven project automatically. When it's finished the Project-Panel should look something like that

![Fresh State after import](./images/IntelliJ_setup/state_after_import.png)


### Configure Smart Tomat

Then we need to install the `Smart Tomcat` plugin in IntelliJ (File -> Settings and then Plugins).

To create a new Run configuration click on `Add configurations...` on the top right side of the IDE.

![Add Configurations](./images/IntelliJ_setup/add_configurations.png)

1. Click on the Plus Symbol and select Smart Tomcat.
2. In the line for Tomcat Server, click on `configure` and select your tomcat folder, save and select the newly configured Tomcat in the run configuration panel
3. In the next line `Deployment..` select the `Node/src/main/webapp` folder
4. Set the context path to `/Node`


Now you should be able to successfully run the configuration by selecting it and clicking the Run button.

![Run Smart Tomcat Configuration](./images/IntelliJ_setup/run_configuration.png)

**If not, run `mvn idea:idea` in the project root and wait until IntelliJ has reloaded the project before you try it again**.