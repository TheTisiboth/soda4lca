![ ](images/soda4LCA_logo_sm.png)

# soda4LCA Developer Guide #


## Integration Tests ##

### Introduction ###

The integration tests are a way to ensure the functionality of all components
of soda4LCA. Because the application consists of several modules (*Node* and 
*Registry* being the two which are actual web applications), after code changes 
it has to be ensured that everything is still working fine, especially 
interactions between the *Node* and *Registry* applications.

For that purpose, a comprehensive suite of integration tests has been created 
that can be run in any environment without requiring prerequisites like a 
servlet container or local database. 


### Prerequisites ###

In addition to the requirement by the application, *Perl* needs to be installed
on the machine where the integration tests are executed. 

On Linux, *libaio1* is required in addition:

`$ sudo apt-get install libaio1`


### Running Integration Tests from Maven ###

#### Node Application ####

For running the integration tests for the *Node* application, just execute the 
Maven goal `verify`, for example by calling

    mvn verify

on the command line. 

The tests will run in an embedded servlet container with an embedded database.


### Running Integration Tests from IDE ###

For running tests in Eclipse against a locally running soda4LCA instance, the 
`persistence.xml` from `src/test/resources/META-INF` needs to be copied to 
`src/main/resources/META-INF` in order to disable JPA caching. 

Run the `testng.xml` file in `src/test/resources` with TestNG. 


### Profiles ###

For headless execution, use the `headless` profile (-Pheadless argument).


### Docker images ###

To build the docker image, run

    mvn clean package -pl Node -am -Pdocker_image

and

    cd Node 
    mvn dockerfile:push@docker
    
to push it. 


To build the docker image for a test instance, run

    mvn pre-integration-test -Pdocker_image_test

    cd Node 
    mvn dockerfile:build@docker-test
    mvn dockerfile:tag@docker-test

and

    mvn dockerfile:push@docker-test
    
to push it. 


## Source Code ##

### Node Component ###

Node packages:

- `configuration`

- `model.*`

- `model.dao`

- `persistence`

- `security`

- `services`

- `util`

- `webgui`

- `xml.read`