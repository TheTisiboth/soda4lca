[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/okusche/soda4lca/src/6.x-branch/)  [![okworx/soda4lca on Docker Hub](https://img.shields.io/badge/docker-ready-blue.svg)](https://hub.docker.com/r/okworx/soda4lca)

![ ](https://bitbucket.org/okusche/soda4lca/raw/9ed83d67f5307c3d2a6b6902d1cbba0a77beb9c9/Doc/src/Administration_Guide/images/soda4LCA_logo_sm.png) 

# soda4LCA 6.4.2

## License

Unless stated otherwise, all source code of the soda4LCA project is licensed under the GNU Affero General Public License, version 3 (AGPL). Please see the [LICENSE.txt](LICENSE.txt) file in the root directory of the source code.


## Documentation

Browse the following documentation online:

[Release Notes](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/RELEASE_NOTES.md) 

[Installation Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Installation_Guide/Installation_Guide.md)

[Configuration Options Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Installation_Guide/Configuration_Options_Guide.md)

[GDPR Compliance Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Installation_Guide/GDPR_Compliance_Guide.md)

[Administration Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Administration_Guide/Administration_Guide.md)

[User Guide (Node)](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Node_User_Guide/Node_User_Guide.md)

[GLAD Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Administration_Guide/GLAD_Guide.md)

[FAQ](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/FAQ/FAQ.md)

[Service API](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Service_API/Service_API.md)

Developer Guides:

[Developer Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Developer_Guide/Developer_Guide.md)

[Eclipse IDE Setup](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Developer_Guide/Eclipse_Setup.md)

[Gitpod Guide](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/Developer_Guide/Gitpod_Guide.md)


## Mailing List

There is a public mailing list for announcements and discussion that you can subscribe to by sending an email to

```
soda4lca-request@lists.kit.edu
```

or by following the instructions at [https://www.lists.kit.edu/wws/info/soda4lca](https://www.lists.kit.edu/wws/info/soda4lca).


## Commercial Support & Application Hosting

Commercial support, training and application hosting (Software as a Service, SaaS) is available through [ok*worx](https://www.okworx.com/).


## Contributing

Code contributions (bug fixes, new features, improvements) are very welcome! Please submit a pull request that's based on the latest code in to ensure quick processing without merge conflicts.

If you plan to implement a new feature or make an improvement, please get in touch with the development lead [Oliver Kusche](https://www.okworx.com/) beforehand to make sure your implementation is in line with the software's architecture. He'll be glad to assist you! 


## Contributors

see [CONTRIBUTORS.txt](https://bitbucket.org/okusche/soda4lca/src/6.4.2/Doc/src/CONTRIBUTORS.txt)
